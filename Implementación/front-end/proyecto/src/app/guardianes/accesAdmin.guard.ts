import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { environment } from 'src/environments/environment';
import { TokenStorageServicio } from '../servicios/token-storage.service';

const ROL1 = environment.ROL1;

@Injectable()
export class AccesAdminGuard implements CanActivate {
    constructor(private router: Router,
        private tokenStorageServicio: TokenStorageServicio) { }

    roles: string[] = [];
 
    canActivate(): boolean {
        const user = this.tokenStorageServicio.leerUsuario();
        this.roles = user.roles;
    
        if (this.roles[0] == ROL1[0]) {
            return true;
        } else {
            this.router.navigateByUrl('/');
            return false;
        }
    }
}