import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { TokenStorageServicio } from '../servicios/token-storage.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private router: Router,
    private tokenStorageServicio: TokenStorageServicio) { }

    isLoggedIn = false;

  canActivate(): boolean {
    this.isLoggedIn = !!this.tokenStorageServicio.leerToken();

    if (this.isLoggedIn) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}