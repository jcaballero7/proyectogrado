import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarUsuarioComponent } from './paginas/editar-usuario/editar-usuario.component';
import { EditarProyectoComponent } from './paginas/editar-proyecto/editar-proyecto.component';
import { InicioComponent } from './compartido/inicio/inicio.component';
import { LoginComponent } from './compartido/login/login.component';
import { ProyectosComponent } from './paginas/proyectos/proyectos.component';
import { UsuariosComponent } from './paginas/usuarios/usuarios.component';
import { SeguimientoComponent } from './paginas/seguimientos/seguimientos.component';
import { FechasHabilesComponent } from './paginas/fechas-habiles/fechas-habiles.component';
import { LoginGuard } from './guardianes/login.guard';
import { VerSeguimientoComponent } from './paginas/ver-seguimiento/ver-seguimiento.component';
import { EditarFechasComponent } from './paginas/editar-fechas/editar-fechas.component';
import { AccesAdminGuard } from './guardianes/accesAdmin.guard';
import { AccesDirecGuard } from './guardianes/accesDirec.guard';
import { ProgramasComponent } from './paginas/programas/programas.component';
import { EditarProgramaComponent } from './paginas/editar-programa/editar-programa.component';
import { CambiarContrasenaComponent } from './paginas/cambiar-contrasena/cambiar-contrasena.component';

const routes: Routes = [
  { path: 'fechas', component: FechasHabilesComponent, canActivate: [LoginGuard, AccesAdminGuard] },
  { path: 'fechas/editar/:id', component: EditarFechasComponent, canActivate: [LoginGuard, AccesAdminGuard] },

  { path: 'usuarios', component: UsuariosComponent, canActivate: [LoginGuard, AccesAdminGuard] },
  { path: 'usuario/editar/:correo', component: EditarUsuarioComponent, canActivate: [LoginGuard, AccesAdminGuard] },

  { path: 'proyectos', component: ProyectosComponent, canActivate: [LoginGuard, AccesDirecGuard] },
  { path: 'proyecto/editar/:cod_proyecto', component: EditarProyectoComponent, canActivate: [LoginGuard, AccesAdminGuard] },

  { path: 'seguimientos', component: SeguimientoComponent, canActivate: [LoginGuard] },
  { path: 'seguimiento/ver/:id_seguimiento', component: VerSeguimientoComponent, canActivate: [LoginGuard] },

  { path: 'programas', component: ProgramasComponent, canActivate: [LoginGuard, AccesAdminGuard] },
  { path: 'programa/editar/:id', component: EditarProgramaComponent, canActivate: [LoginGuard, AccesAdminGuard] },

  { path: 'cambiar-contrasena', component: CambiarContrasenaComponent, canActivate: [LoginGuard] },



  
  { path: 'login', component: LoginComponent },
  { path: '', component: InicioComponent, canActivate: [LoginGuard] },

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
