import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

import { LoginComponent } from './compartido/login/login.component';
import { CabeceroComponent } from './compartido/cabecero/cabecero.component';
import { PiePaginaComponent } from './compartido/pie-pagina/pie-pagina.component';
import { LoginServicio } from './servicios/login.service';
import { UsuariosComponent } from './paginas/usuarios/usuarios.component';
import { UsuarioServicio } from './servicios/usuario.service';
import { ProyectosComponent } from './paginas/proyectos/proyectos.component';
import { InicioComponent } from './compartido/inicio/inicio.component';
import { ProyectoServicio } from './servicios/proyecto.service';
import { EditarProyectoComponent } from './paginas/editar-proyecto/editar-proyecto.component';
import { EditarUsuarioComponent } from './paginas/editar-usuario/editar-usuario.component';
import { SeguimientoComponent } from './paginas/seguimientos/seguimientos.component';
import { SeguimientoServicio } from './servicios/seguimiento.service';
import { FechasHabilesComponent } from './paginas/fechas-habiles/fechas-habiles.component';
import { LoginGuard } from './guardianes/login.guard';
import { VerSeguimientoComponent } from './paginas/ver-seguimiento/ver-seguimiento.component';
import { TokenStorageServicio } from './servicios/token-storage.service';
import { authInterceptorProviders } from './ayudantes/auth.interceptor';
import { FechasHabilesServicio } from './servicios/fechas-habiles.service';
import { EditarFechasComponent } from './paginas/editar-fechas/editar-fechas.component';
import { AccesAdminGuard } from './guardianes/accesAdmin.guard';
import { AccesDirecGuard } from './guardianes/accesDirec.guard';
import { ProgramasComponent } from './paginas/programas/programas.component';
import { ProgramasServicio } from './servicios/programas.service';
import { EditarProgramaComponent } from './paginas/editar-programa/editar-programa.component';
import { CambiarContrasenaComponent } from './paginas/cambiar-contrasena/cambiar-contrasena.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsuariosComponent,
    ProyectosComponent,
    CabeceroComponent,
    PiePaginaComponent,
    InicioComponent,
    EditarProyectoComponent,
    EditarUsuarioComponent,
    SeguimientoComponent,
    FechasHabilesComponent,
    VerSeguimientoComponent,
    EditarFechasComponent,
    ProgramasComponent,
    EditarProgramaComponent,
    CambiarContrasenaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    FlashMessagesModule.forRoot(),
    HttpClientModule,
    NgxPaginationModule, 
    AutocompleteLibModule,
  ],
  providers: [LoginServicio, 
    UsuarioServicio, 
    ProyectoServicio, 
    SeguimientoServicio, 
    TokenStorageServicio, 
    FechasHabilesServicio,
    ProgramasServicio,
    LoginGuard, AccesAdminGuard, AccesDirecGuard, 
    authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
