import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Usuario } from '../../modelos/usuario.model';
import { LoginServicio } from '../../servicios/login.service';
import { TokenStorageServicio } from '../../servicios/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  usuario: Usuario = {
    correo: '',
    contrasena: '',
  }
  correo: string;
  contrasena: string;

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(private router: Router,
    private flashMessages: FlashMessagesService,
    private loginServicio: LoginServicio,
    private tokenStorageServicio: TokenStorageServicio) { }

  ngOnInit() {
    if (this.tokenStorageServicio.leerToken()) {
      this.isLoggedIn = true;
      this.router.navigate(['/']);
      this.roles = this.tokenStorageServicio.leerUsuario().roles;
    }
  }

  login(loginForm: NgForm) {

    if (loginForm.invalid) {
      this.flashMessages.show('Los campos estan vacios, o tienen errores en los campos.', {
        cssClass: 'alert-danger', timeout: 2000
      });
    }
    else {
      this.loginServicio.login(loginForm.value).subscribe(
        resp => {
          this.tokenStorageServicio.guardarToken(resp.token);
          this.tokenStorageServicio.guardarUsuario(resp);

          this.isLoginFailed = false;
          this.isLoggedIn = true;
          this.roles = this.tokenStorageServicio.leerUsuario().roles;
          this.router.navigate(['/']);
          window.location.reload();
        }, (error) => {
          this.flashMessages.show(error.error,
            {
              cssClass: 'alert-danger', timeout: 4000
            });
          this.isLoginFailed = true;
        });
    }
  }
}


