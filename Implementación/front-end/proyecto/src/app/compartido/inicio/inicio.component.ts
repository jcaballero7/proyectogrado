import { Component, OnInit } from '@angular/core';
import { LoginServicio } from '../../servicios/login.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
})
export class InicioComponent implements OnInit {

  content: string;

  constructor(private loginServicio: LoginServicio) { }

  ngOnInit(): void {
    
  }

}
