import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { TokenStorageServicio } from '../../servicios/token-storage.service';

const ROL1 = environment.ROL1;
const ROL2 = environment.ROL2;

@Component({
  selector: 'app-cabecero',
  templateUrl: './cabecero.component.html',
})
export class CabeceroComponent implements OnInit {

  isLoggedIn: boolean;
  usuario: string;
  apellido: string;
  roles: string[] = [];
  isPermitted1: boolean;
  isPermitted2: boolean;
  isPermitted3: boolean;

  constructor(private tokenStorageServicio: TokenStorageServicio,
    private router: Router) { }

  ngOnInit() {
    this.validarInicioSession();
    this.validarAcceso();
  }

  logout(): void {
    this.router.navigate(['login']);
    this.tokenStorageServicio.signOut();
    this.isLoggedIn = false;
  }

  validarInicioSession() {
    this.isLoggedIn = !!this.tokenStorageServicio.leerToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageServicio.leerUsuario();
      this.roles = user.roles;
      this.usuario = user.nombre;
      this.apellido = user.apellido;
    }
  }

  validarAcceso() {
    if (this.roles[0] == ROL1[0]) {
      this.isPermitted1 = true;

    } else if (this.roles[0] !== ROL1[0]) {

      if (this.roles[0] == ROL2[0]) {
        this.isPermitted2 = true;
      }else{
        this.isPermitted3 = true;
      }
     }
  }
}

