import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import Swal from 'sweetalert2';
import { Proyecto } from '../../modelos/proyecto.model';
import { Seguimiento } from '../../modelos/seguimiento.model';
import { ProyectoServicio } from '../../servicios/proyecto.service';
import { SeguimientoServicio } from '../../servicios/seguimiento.service';
import { TokenStorageServicio } from '../../servicios/token-storage.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';



const ROL1 = environment.ROL1;
const ROL2 = environment.ROL2;
@Component({
  selector: 'app-seguimiento',
  templateUrl: './seguimientos.component.html',
})
export class SeguimientoComponent implements OnInit {

  p: number = 1;

  keyword = 'cod_proyecto';

  proyectos: Proyecto[];
  proyecto: Proyecto = {
    cod_proyecto: '',
    titulo: '',
    programa: '',
    hor_asesoria: '',
    cor_director: '',
    can_estudiantes: 0,
    cor_estudiante1: '',
    cor_estudiante2: '',
    cor_estudiante3: '',
    tip_proyecto: '',
    estado: true,
  }

  seguimientos!: Seguimiento[];
  seguimiento: Seguimiento = {
    id_seguimiento: 0,
    cod_proyecto: '',
    hor_asesoria: '',
    pendientes: '',
    des_tematica: '',
    hallazgos: '',
    cor_director: '',
    ace_director: false,
    asi_estudiante1: true,
    asi_estudiante2: true,
    asi_estudiante3: true,
    asi_director: true,
  }



  isLoggedIn: boolean;
  roles: string[] = [];
  isPermitted1: boolean;
  isPermitted2: boolean;
  isPermitted3: boolean;
  pendientes: any;
  hor_asesoria: any;


  @ViewChild("seguimientoForm") seguimientoForm: NgForm;
  @ViewChild("proyectoForm") proyectoForm: NgForm;

  @ViewChild("botonCerrar") botonCerrar: ElementRef;


  constructor(private seguimientoServicio: SeguimientoServicio,
    private flasgMessages: FlashMessagesService,
    private proyectoServicio: ProyectoServicio,
    private tokenStorageServicio: TokenStorageServicio,
    public changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    ) { }

  ngOnInit(): void {
    this.validarInicioSession();
    this.validarAcceso();
    this.consultaProyectos();
    this.consultarSeguimientos();
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  handlePageChange(event: any) {
    this.p = event;
    this.consultarSeguimientos();
  }

  agregar(seguimientoForm: NgForm) {

    if (seguimientoForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario vacíos o con errores.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.seguimientoServicio.registrarSeguimiento(seguimientoForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Seguimiento registrado.',
          'success'
        )
          .then(() => {
            this.seguimientoForm!.resetForm();
            this.cerrarModal();
            this.router.navigate(['/seguimientos']);
            window.location.reload();
          });
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Seguimiento no registrado . ' + err.error,
          'error'
        )
      });
    }
  }

  private cerrarModal() {
    this.botonCerrar!.nativeElement.click();
  }

  consultarSeguimientos(): void {
    this.seguimientoServicio.leerSeguimientos().subscribe(
      datos => {
        this.seguimientos = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Seguimientos no consultados. ' + error.message,
          'error'
        )
      });
  }

  consultaProyectos(): void {
    this.proyectoServicio.leerProyectosSinAval().subscribe(
      datos => {
        this.proyectos = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Proyectos no consultados. ' + error.message,
          'error'
        )
      });
  }

  validarInicioSession() {
    this.isLoggedIn = !!this.tokenStorageServicio.leerToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageServicio.leerUsuario();
      this.roles = user.roles;
    }
  }

  validarAcceso() {

    if (this.roles[0] == ROL1[0]) {
      this.isPermitted1 = true;

    } else if (this.roles[0] !== ROL1[0]) {

      if (this.roles[0] == ROL2[0]) {
        this.isPermitted2 = true;
      } else {
        this.isPermitted3 = false;
      }
    }
  }

  cargarInformacion(proyectoForm: NgForm) {
    const codigo = proyectoForm.value.cod_proyecto.cod_proyecto;
    this.seguimientoServicio.leerPendientes(codigo).subscribe(
      (datos: any) => {
        this.seguimiento.pendientes = datos[0].pendientes;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Pendientes no consultados. ' + error.message,
          'error'
        )
      });
    //
    this.seguimientoServicio.leerHorasesoria(codigo).subscribe(
      (datos: any) => {
        this.proyecto.hor_asesoria = datos[0].hor_asesoria;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Hora de asesoria no consultada. ' + error.message,
          'error'
        )
      });
    // Cargar información del proyecto
    this.proyecto = this.proyectoForm.value.cod_proyecto;
  }
}
