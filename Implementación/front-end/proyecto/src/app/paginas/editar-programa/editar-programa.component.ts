import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Programa } from 'src/app/modelos/programa.model';
import { ProgramasServicio } from 'src/app/servicios/programas.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-programa',
  templateUrl: './editar-programa.component.html',
})
export class EditarProgramaComponent implements OnInit {


  programa: Programa = {
    id: 0,
    nombre: ''
  }
  id:number;

  
  constructor(private programaServicio: ProgramasServicio,
    private flasgMessages: FlashMessagesService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.consultarPrograma();
  }

  guardarUsuario(programaForm: NgForm) {
    if (programaForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario vacíos.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.programaServicio.modificarPrograma(this.id, programaForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Programa actualizado.',
          'success'
        )
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Programa no actualizado. ' + err.message,
          'error'
        )
      })
    };
  }

  consultarPrograma(): void {
    this.programaServicio.leerPrograma(this.id).subscribe(
      datos => {
        this.programa = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Usuarios no consultado. ' + error.message,
          'error'
        )
      });

  }


}
