import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import Swal from 'sweetalert2';
import { Proyecto } from '../../modelos/proyecto.model';
import { ProyectoServicio } from '../../servicios/proyecto.service';
import { ProgramasServicio } from 'src/app/servicios/programas.service';

@Component({
  selector: 'app-editar-proyecto',
  templateUrl: './editar-proyecto.component.html',
})
export class EditarProyectoComponent implements OnInit {
  // Para la busqueda autocompletada
  keyword = 'correo';
  proyecto: Proyecto = {
    cod_proyecto: '',
    titulo: '',
    programa: '',
    hor_asesoria: '',
    can_estudiantes: 0,
    cor_estudiante1: '',
    cor_estudiante2: '',
    cor_estudiante3: '',
    tip_proyecto: '',
    estado: true,
  }

  tipo_proyectos: string[] = ["Proyecto de grado 1", "Proyecto de grado 2"];

  cod_proyecto: string;

  directores: any[];
  estudiantes: any[];
  programas: any[];

  constructor(private proyectoServicio: ProyectoServicio,
    private programasServicio: ProgramasServicio,
    private flasgMessages: FlashMessagesService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.cod_proyecto = this.route.snapshot.params['cod_proyecto'];
    this.consultarProyecto();
    this.consultarDirectores();
    this.consultarEstudiantes();
    this.consultarProgramas();
  }


  guardarProyecto(proyectoForm: NgForm) {
    if (proyectoForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario vacíos.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {      
      this.proyectoServicio.modificarProyecto(this.cod_proyecto, proyectoForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Proyecto actualizado.',
          'success'
        ) 
      }, (err) => {
        Swal.fire(
          'Fallido!',
          'Proyecto no actualizado. ' + err.message,
          'error'
        )                 
        })
      };
  }

  consultarProyecto(): void {
    this.proyectoServicio.leerProyecto(this.cod_proyecto).subscribe(
      datos => {
        this.proyecto = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Proyecto no consultado. ' + error.message,
          'error'
        )
      });
  }

  consultarDirectores(): void {
    this.proyectoServicio.leerDirectores().subscribe(
      datos => {
        this.directores = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Directores no consultados. ' + error.message,
          'error'
        )
      });
  }

  consultarEstudiantes(): void {
    this.proyectoServicio.leerEstudiantes().subscribe(
      datos => {
        this.estudiantes = datos;        
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Estudiantes no consultados. ' + error.message,
          'error'
        )
      });
  }

  consultarProgramas() {
    this.programasServicio.leerProgramas().subscribe(
      datos => {
        this.programas = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Fechas no consultadas. ' + error.message,
          'error'
        )
      });
  }

}
