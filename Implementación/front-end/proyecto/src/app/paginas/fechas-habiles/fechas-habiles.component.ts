import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import Swal from 'sweetalert2';
import { Fechas_Habiles } from '../../modelos/fechas-habiles.model';
import { FechasHabilesServicio } from '../../servicios/fechas-habiles.service';

@Component({
  selector: 'app-fechas-habiles',
  templateUrl: './fechas-habiles.component.html',
})
export class FechasHabilesComponent implements OnInit {

  constructor(private flasgMessages: FlashMessagesService,
    private fechasHabilesServicio: FechasHabilesServicio,
    private router: Router) { }
  p: number = 1;

  fechas_habiles: Fechas_Habiles[];
  fecha_habil: Fechas_Habiles = {
    id: 0,
    fec_ini: '',
    fec_fin: '',
    descripcion: '',
  }

  permitir: boolean;

  @ViewChild("fechasForm") fechasForm: NgForm;
  @ViewChild("botonCerrar") botonCerrar: ElementRef;

  ngOnInit(): void {
    this.consultarFechas();
    if(this.fechas_habiles){
      console.log("Hola");
    }
  }

  handlePageChange(event: any) {
    this.p = event;
    this.consultarFechas();
  }

  agregar(fechasForm: NgForm) {
    if (fechasForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario vacíos o con errores.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.fechasHabilesServicio.agregarFecha(fechasForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Fecha creada.',
          'success'
        )
          .then(() => {
            this.fechasForm!.resetForm();
            this.cerrarModal();
            this.router.navigate(['/fechas']);
            window.location.reload();
          });
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Fecha no creada. ' + err.message,
          'error'
        )
      });
    }
  }

  consultarFechas(): void {
    this.fechasHabilesServicio.leerFechas().subscribe(
      datos => {
        this.fechas_habiles = datos;
        if(this.fechas_habiles.length == 0){
          this.permitir = true;
        }
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Fechas no consultadas. ' + error.message,
          'error'
        )
      });
  }

  private cerrarModal() {
    this.botonCerrar!.nativeElement.click();
  }

}
