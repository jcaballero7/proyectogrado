import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import Swal from 'sweetalert2';
import { Proyecto } from '../../modelos/proyecto.model';
import { ProyectoServicio } from '../../servicios/proyecto.service';
import { TokenStorageServicio } from '../../servicios/token-storage.service';
import { environment } from 'src/environments/environment';
import { ProgramasServicio } from 'src/app/servicios/programas.service';

const ROL1 = environment.ROL1;
const ROL2 = environment.ROL2;

@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
})
export class ProyectosComponent implements OnInit {

  // Para la busqueda autocompletada
  keyword = 'correo';

  p: number = 1;
  proyectos: Proyecto[];
  proyecto: Proyecto = {
    cod_proyecto: '',
    titulo: '',
    programa: '',
    hor_asesoria: '',
    cor_director: '',
    can_estudiantes: 1,
    cor_estudiante1: '',
    cor_estudiante2: '',
    cor_estudiante3: '',
    tip_proyecto: '',
    estado: true,
    aval:false,
  }

  tipo_proyectos: string[] = ["Proyecto de grado 1", "Proyecto de grado 2"];


  directores: any[];
  estudiantes: any[];
  programas: any[];

  isLoggedIn: boolean;
  roles: string[] = [];
  isPermitted1: boolean;
  isPermitted2: boolean;
  isPermitted3: boolean;

  avalado = true;
  noAvalado = false;


  @ViewChild("proyectoForm") proyectoForm: NgForm;
  @ViewChild("botonCerrar") botonCerrar: ElementRef;


  constructor(private proyectoServicio: ProyectoServicio,
    private programasServicio: ProgramasServicio,
    private flasgMessages: FlashMessagesService,
    private router: Router,
    private tokenStorageServicio: TokenStorageServicio) { }

  ngOnInit(): void {
    this.validarInicioSession();
    this.validarAcceso();
    this.consultarProyectos();
    this.consultarDirectores();
    this.consultarEstudiantes();
    this.consultarProgramas();
  }

  handlePageChange(event: any) {
    this.p = event;
    this.consultarProyectos();
  }

  agregar(proyectoForm: NgForm) {
    if (proyectoForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario vacíos o con errores.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.proyectoServicio.agregarProyecto(proyectoForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Proyecto creado.',
          'success'
        )
          .then(() => {
            this.proyectoForm!.resetForm();
            this.cerrarModal();
            this.router.navigate(['/proyectos']);
            window.location.reload();
          });
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Proyecto no creado. ' + err.error,
          'error'
        )
      });
    }
  }

  cerrarModal() {
    this.botonCerrar!.nativeElement.click();
  }

  consultarDirectores() {
    this.proyectoServicio.leerDirectores().subscribe(
      datos => {
        this.directores = datos;

      },
      error => {
        Swal.fire(
          'Fallido!',
          'Directores no consultados. ' + error.error,
          'error'
        )
      });
  }

  consultarEstudiantes() {
    this.proyectoServicio.leerEstudiantes().subscribe(
      datos => {
        this.estudiantes = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Estudiantes no consultados. ' + error.error,
          'error'
        )
      });
  }

  consultarProyectos() {
    this.proyectoServicio.leerProyectos().subscribe(
      datos => {
        this.proyectos = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Proyectos no consultados. ' + error.error,
          'error'
        )
      });
  }

  consultarProgramas() {
    this.programasServicio.leerProgramas().subscribe(
      datos => {
        this.programas = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Fechas no consultadas. ' + error.message,
          'error'
        )
      });
  }

  validarInicioSession() {
    this.isLoggedIn = !!this.tokenStorageServicio.leerToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageServicio.leerUsuario();
      this.roles = user.roles;
    }
  }

  validarAcceso() {
    if (this.roles[0] == ROL1[0]) {
      this.isPermitted1 = true;

    } else if (this.roles[0] !== ROL1[0]) {

      if (this.roles[0] == ROL2[0]) {
        this.isPermitted2 = false;
      } else {
        this.isPermitted3 = false;
      }
    }
  }

  avalar(cod_proyecto: any) {
    Swal.fire({
      title: '¿Está seguro?',
      text: "No podra revertirlo!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar.',
      confirmButtonText: 'Sí, avalar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.proyectoServicio.evaluarProyecto(cod_proyecto, this.avalado).subscribe(resp => {
          Swal.fire(
            'Correcto!',
            'El proyecto ha sido avalado.',
            'success'
          )
          .then(() => {
            window.location.reload();
          });
        }, (err) => {

          //Si sucede error
          Swal.fire(
            'Fallido!',
            'Proyecto no avalado. ' + err.error,
            'error'
          )
        });
      }
    }
    )
  }

  noAvalar(cod_proyecto: any) {
    Swal.fire({
      title: '¿Está seguro?',
      text: "No podra revertirlo!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar.',
      confirmButtonText: 'Sí, no avalar!'
    }).then((result) => {
      if (result.isConfirmed) this.proyectoServicio.evaluarProyecto(cod_proyecto, this.noAvalado).subscribe(resp => {
        Swal.fire(
          'Correcto!',
          'El proyecto no se ha avalado.',
          'success'
        )
        .then(() => {
          window.location.reload();
        });
      }, (err) => {

        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Fallo el proceso de no avalar el proyecto. ' + err.error,
          'error'
        )
      });
    }
    )
  }
}
