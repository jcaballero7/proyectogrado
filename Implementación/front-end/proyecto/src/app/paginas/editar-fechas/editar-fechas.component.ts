import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import Swal from 'sweetalert2';
import { Fechas_Habiles } from '../../modelos/fechas-habiles.model';
import { FechasHabilesServicio } from '../../servicios/fechas-habiles.service';

@Component({
  selector: 'app-editar-fechas',
  templateUrl: './editar-fechas.component.html',
})
export class EditarFechasComponent implements OnInit {

  constructor(private flasgMessages: FlashMessagesService,
    private route: ActivatedRoute,
    private fechasHabilesServicio: FechasHabilesServicio) { }

  fechas_habiles: Fechas_Habiles[];
  fecha_habil: Fechas_Habiles = {
    id: 0,
    fec_ini: '',
    fec_fin: '',
    descripcion: '',
  }

  id:number;

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.consultarFecha();
  }


  guardarFecha(fechaForm: NgForm) {
    if (fechaForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario vacíos.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.fechasHabilesServicio.modificarFecha(this.id, fechaForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Fecha actualizado.',
          'success'
        )
      }, (err) => {
        Swal.fire(
          'Fallido!',
          'Fecha no actualizado. ' + err.message,
          'error'
        )
      })
    };
  }

  consultarFecha(): void {
    this.fechasHabilesServicio.leerFecha(this.id).subscribe(
      datos => {
        this.fecha_habil = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Fecha no consultado. ' + error.message,
          'error'
        )
      });
  }
}
