import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Programa } from 'src/app/modelos/programa.model';
import { ProgramasServicio } from 'src/app/servicios/programas.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
})
export class ProgramasComponent implements OnInit {


  p: number = 1;

  programas: Programa[];
  programa: Programa = {
    id: 0,
    nombre: ''
  }

  @ViewChild("programaForm") programaForm: NgForm;
  @ViewChild("botonCerrar") botonCerrar: ElementRef;
  handlePageChange(event: any) {
    this.p = event;
    this.consultarProgramas();
  }


  constructor(private flasgMessages: FlashMessagesService,
    private programaServicio: ProgramasServicio,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.consultarProgramas();
  }

  agregar(programaForm: NgForm) {
    if (programaForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario vacíos o con errores.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.programaServicio.agregarPrograma(programaForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Programa registrado.',
          'success'
        )
          .then(() => {
            this.programaForm!.resetForm();
            this.cerrarModal();
            this.router.navigate(['/programas']);
            window.location.reload();
          });
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Programa no registrado. ' + err.message,
          'error'
        )
      });
    }
  }

  consultarProgramas() {
    this.programaServicio.leerProgramas().subscribe(
      datos => {
        this.programas = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Fechas no consultadas. ' + error.message,
          'error'
        )
      });
  }

  private cerrarModal() {
    this.botonCerrar!.nativeElement.click();
  }
}
