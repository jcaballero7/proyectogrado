import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Seguimiento } from '../../modelos/seguimiento.model';
import { SeguimientoServicio } from '../../servicios/seguimiento.service';
import { environment } from 'src/environments/environment';
import { TokenStorageServicio } from '../../servicios/token-storage.service';
import { FechasHabilesServicio } from 'src/app/servicios/fechas-habiles.service';
import { Fechas_Habiles } from 'src/app/modelos/fechas-habiles.model';


const ROL1 = environment.ROL1;
const ROL2 = environment.ROL2;
@Component({
  selector: 'app-ver-seguimiento',
  templateUrl: './ver-seguimiento.component.html',
})
export class VerSeguimientoComponent implements OnInit {

  p: number = 1;
  seguimiento: Seguimiento = {
    id_seguimiento: 0,
    cod_proyecto: '',
    hor_asesoria: '',
    des_tematica: '',
    hallazgos: '',
    cor_director: '',
    ace_director: false,
    cor_estudiante1: '',
    ace_estudiante1: false,
    com_estudiante1: '',
    cor_estudiante2: '',
    ace_estudiante2: false,
    com_estudiante2: '',
    cor_estudiante3: '',
    ace_estudiante3: false,
    com_estudiante3: '',
    asi_estudiante1: false,
    asi_estudiante2: false,
    asi_estudiante3: false,
  }


  fechas_habiles: Fechas_Habiles[];
  fecha_habil: Fechas_Habiles = {
    id: 0,
    fec_ini: '',
    fec_fin: '',
    descripcion: '',
  }

  id_seguimiento: string;

  isLoggedIn: boolean;
  roles: string[] = [];
  isPermitted1: boolean;
  isPermitted2: boolean;
  isPermitted3: boolean;


  archivoSubido: Array<File>;
  archivos: any[];
  archivo: any = {
    anexo: '',
    url: ''
  }

  constructor(private seguimientoServicio: SeguimientoServicio,
    private route: ActivatedRoute,
    private tokenStorageServicio: TokenStorageServicio,
    private router: Router,
    private fechasHabilesServicio: FechasHabilesServicio) { }

  ngOnInit(): void {
    this.id_seguimiento = this.route.snapshot.params['id_seguimiento'];
    this.consultarSeguimiento();
    this.validarInicioSession();
    this.validarAcceso();
    this.consultarFechas();

  }

  guardarSeguimiento() {

    if (this.seguimiento.pro_evaluado == true) {
      Swal.fire(
        'No es posible!',
        'El proyecto de este seguimiento ya fue evaluado.',
        'warning'
      )
    } else if (this.validarFechas()) {

      let formData = new FormData();

      if (this.seguimiento.cor_estudiante2) {

        if (this.seguimiento.ace_estudiante1) {
          formData.set("ace_estudiante1", this.seguimiento.ace_estudiante1!.toString());
        }
        if (this.seguimiento.com_estudiante1) {
          formData.set("com_estudiante1", this.seguimiento.com_estudiante1!);
        }
        formData.set("asi_estudiante1", this.seguimiento.asi_estudiante1!.toString());

        /////// Estudiante 2 /////////////
        if (this.seguimiento.ace_estudiante2) {
          formData.set("ace_estudiante2", this.seguimiento.ace_estudiante2!.toString());
        }
        if (this.seguimiento.com_estudiante2) {
          formData.set("com_estudiante2", this.seguimiento.com_estudiante2!);
        }
        formData.set("asi_estudiante2", this.seguimiento.asi_estudiante2!.toString());
      }
      else if (this.seguimiento.cor_estudiante3) {


        if (this.seguimiento.ace_estudiante1) {
          formData.set("ace_estudiante1", this.seguimiento.ace_estudiante1!.toString());
        }
        if (this.seguimiento.com_estudiante1) {
          formData.set("com_estudiante1", this.seguimiento.com_estudiante1!);
        }
        formData.set("asi_estudiante1", this.seguimiento.asi_estudiante1!.toString());

        ///Estudiante 2///////////////

        if (this.seguimiento.ace_estudiante2) {
          formData.set("ace_estudiante2", this.seguimiento.ace_estudiante2!.toString());
        }
        if (this.seguimiento.com_estudiante2) {
          formData.set("com_estudiante2", this.seguimiento.com_estudiante2!);
        }
        formData.set("asi_estudiante2", this.seguimiento.asi_estudiante2!.toString());

        ///Estudiante 3 ///////////
        if (this.seguimiento.com_estudiante3) {
          formData.set("com_estudiante3", this.seguimiento.com_estudiante3!);
        }
        formData.set("asi_estudiante1", this.seguimiento.asi_estudiante1!.toString());
        if (this.seguimiento.com_estudiante2) {
          formData.set("com_estudiante2", this.seguimiento.com_estudiante2!);
        }
      }
      else {
        if (this.seguimiento.ace_estudiante1) {

          formData.set("ace_estudiante1", this.seguimiento.ace_estudiante1!.toString());
        }
        if (this.seguimiento.com_estudiante1) {
          formData.set("com_estudiante1", this.seguimiento.com_estudiante1!);
        }
        formData.set("asi_estudiante1", this.seguimiento.asi_estudiante1!.toString());

      }

      if (this.archivoSubido)
        for (var i = 0; i < this.archivoSubido.length; i++) {
          formData.append("archivo", this.archivoSubido[i], this.archivoSubido[i].name);
        }
      this.seguimientoServicio.modificarSeguimiento(this.id_seguimiento, formData).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Seguimiento actualizado.',
          'success'
        )
          .then(() => {
            this.router.navigate(['/seguimientos']);
          });
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Seguimiento no actualizado. ' + err.error,
          'error'
        )
      })
    } else {
      Swal.fire(
        'No es posible!',
        'La fecha habilitada ha finalizado.',
        'warning'
      )
    }
  }

  consultarSeguimiento(): void {
    this.seguimientoServicio.leerSeguimiento(this.id_seguimiento).subscribe(
      (datos: any) => {
        this.seguimiento = datos.seguimiento;
        this.archivos = datos.anexo;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Seguimiento no consultado. ' + error.error
          ,
          'error'
        )
      });
  }


  validarInicioSession() {
    this.isLoggedIn = !!this.tokenStorageServicio.leerToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageServicio.leerUsuario();
      this.roles = user.roles;
    }
  }

  validarAcceso() {

    if (this.roles[0] == ROL1[0]) {
      this.isPermitted1 = true;

    } else if (this.roles[0] !== ROL1[0]) {

      if (this.roles[0] == ROL2[0]) {
        this.isPermitted2 = true;
      } else {
        this.isPermitted3 = true;
      }
    }
  }

  fileChange(event: any) {
    this.archivoSubido = event.target.files;
  }

  consultarFechas() {
    this.fechasHabilesServicio.leerFechas().subscribe(
      datos => {
        this.fechas_habiles = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Fechas no consultadas. ' + error.message,
          'error'
        )
      });
  }

  validarFechas(): any {
    if (this.seguimiento.hor_asesoria! > this.fechas_habiles[0].fec_ini! && this.seguimiento.hor_asesoria! < this.fechas_habiles[0].fec_fin!) {
      return true;
    } else {
      return false;
    }
  }
}
