import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Usuario } from 'src/app/modelos/usuario.model';
import { UsuarioServicio } from 'src/app/servicios/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cambiar-contrasena',
  templateUrl: './cambiar-contrasena.component.html',
})
export class CambiarContrasenaComponent implements OnInit {


  usuario: Usuario = {
    contrasena: '',
    con_nueva: '',
  }
  correo: string;

  @ViewChild("cambiarForm") cambiarForm: NgForm;

  constructor(private flasgMessages: FlashMessagesService,
    private usuarioServicio: UsuarioServicio, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.correo = this.route.snapshot.params['correo'];
  }

  cambiar(cambiarForm: NgForm) {
    if (cambiarForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario con errores.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.usuarioServicio.modificarContrasena(cambiarForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Contraseña actualizada.',
          'success'
        ).then(() => {
          this.cambiarForm!.resetForm();
          this.router.navigate(['/']);
        });
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Contraseña no actualizada. ' + err.message,
          'error'
        )
      })
    };
  }
}
