import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import Swal from 'sweetalert2';
import { Usuario } from '../../modelos/usuario.model';
import { UsuarioServicio } from '../../servicios/usuario.service';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
})
export class EditarUsuarioComponent implements OnInit {

  usuario: Usuario = {
    nombre: '',
    apellido: '',
    correo: '',
    tip_documento: '',
    num_documento: 0,
    telefono: 0,
    rol: '',
    estado: true
  }

  tipo_documentos: string[] = ["CC", "ID", "CE", "PS"];

  rol: string;

  roles: any[];
  correo: string;

  constructor(private usuarioServicio: UsuarioServicio,
    private flasgMessages: FlashMessagesService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.correo = this.route.snapshot.params['correo'];
    this.consultarUsuario();
    this.consultarRoles();
  }

  guardarUsuario(usuarioForm: NgForm) {
    if (usuarioForm.invalid) {
      this.flasgMessages.show('Hay campos del formulario vacíos.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.usuarioServicio.modificarUsuario(this.correo, usuarioForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Usuario actualizado.',
          'success'
        )
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Usuario no actualizado. ' + err.message,
          'error'
        )
      })
    };
  }

  consultarUsuario(): void {
    this.usuarioServicio.leerUsuario(this.correo).subscribe(
      (datos:any) => {
        this.usuario = datos.usuario;
        this.usuario.rol = datos.rol;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Usuarios no consultado. ' + error.message,
          'error'
        )
      });

  }

  consultarRoles(): void {
    this.usuarioServicio.leerRoles().subscribe(
      datos => {
        this.roles = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'Roles no consultados. ' + error.message,
          'error'
        )
      });
  }
}
