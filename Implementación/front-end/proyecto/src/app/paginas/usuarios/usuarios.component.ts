import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from "@angular/forms";
import { UsuarioServicio } from '../../servicios/usuario.service';
import { Usuario } from '../../modelos/usuario.model';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
})
export class UsuariosComponent implements OnInit {
  p: number = 1;
  usuarios: Usuario[];
  usuario: Usuario = {
    correo: '',
    contrasena: '',
    nombre: '',
    apellido: '',
    tip_documento: '',
    num_documento: 0,
    telefono: 0,
    rol: '',
    estado: true
  }

  tipos_documentos: string[] = ["CC", "ID", "CE", "PS"];
  roles: any[];

  content: string;

  @ViewChild("usuarioForm") usuarioForm: NgForm;
  @ViewChild("botonCerrar") botonCerrar: ElementRef;


  constructor(private usuarioServicio: UsuarioServicio,
    private flashMessages: FlashMessagesService,
    private router: Router) { }

  ngOnInit(): void {

    this.consultarUsuarios();
    this.consultarRoles();
  }

  handlePageChange(event: any) {
    this.p = event;
    this.consultarUsuarios();
  }

  agregar(usuarioForm: NgForm) {
    if (usuarioForm.invalid) {
      this.flashMessages.show('Hay campos del formulario vacíos o con errores.', {
        cssClass: 'alert-danger', timeout: 4000
      });
    }
    else {
      this.usuarioServicio.agregarUsuario(usuarioForm.value).subscribe(resp => {
        Swal.fire(
          'Exitoso!',
          'Usuario creado.',
          'success'
        )
          .then(() => {
            this.limpiarModal();
            this.cerrarModal();
            this.router.navigate(['/usuarios']);
            window.location.reload();
          });
      }, (err) => {
        //Si sucede error
        Swal.fire(
          'Fallido!',
          'Usuario no creado. ' + err.error,
          'error'
        )
      });
    }
  }

  private cerrarModal() {
    this.botonCerrar!.nativeElement.click();
  }

  consultarUsuarios(): void {
    this.usuarioServicio.leerUsuarios().subscribe(
      datos => {
        this.usuarios = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'No se consultaron los usuarios. ' + error.message,
          'error'
        )
      });
  }

  consultarRoles(): void {
    this.usuarioServicio.leerRoles().subscribe(
      datos => {
        this.roles = datos;
      },
      error => {
        Swal.fire(
          'Fallido!',
          'No se consultaron los roles. ' + error.message,
          'error'
        )
      });
  }

  limpiarModal() {
    this.usuarioForm!.resetForm();
  }
}
