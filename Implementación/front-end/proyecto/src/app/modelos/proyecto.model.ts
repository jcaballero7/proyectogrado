export class Proyecto{
    cod_proyecto?:string;
    titulo?:string;
    programa?:string;
    hor_asesoria?:string;
    cor_director?:string;
    can_estudiantes?:number;
    cor_estudiante1?:string;
    cor_estudiante2?:string;
    cor_estudiante3?:string;
    tip_proyecto?:string;
    estado?:boolean;
    aval?:boolean;
  }
  