export class Seguimiento{
    id_seguimiento?:number;
    cod_proyecto?:string;
    hor_asesoria?:string;
    pendientes?:string;
    des_tematica?:string;
    hallazgos?:string;
    cor_director?:string;
    ace_director?:boolean;
    cor_estudiante1?:string;
    ace_estudiante1?:boolean;
    com_estudiante1?:string;
    cor_estudiante2?:string;
    ace_estudiante2?:boolean;
    com_estudiante2?:string;
    cor_estudiante3?:string;
    ace_estudiante3?:boolean;
    com_estudiante3?:string;
    asi_estudiante2?:boolean;
    asi_estudiante1?:boolean;  
    asi_estudiante3?:boolean;
    pro_evaluado?:boolean;
    asi_director?:boolean;
  }
  