export class Usuario{
    correo?:string;  
    contrasena?:string;
    con_nueva?:string;
    nombre?:string;
    apellido?:string;
    tip_documento?:string;
    num_documento?:number;
    telefono?:number;
    rol?:string;
    estado?:boolean;
  }
  