import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { TokenStorageServicio } from './token-storage.service';

const base_url = environment.base_url;

@Injectable()
export class LoginServicio {
  userToken: any;
  constructor(private http: HttpClient,
    private tokenStorageServicio: TokenStorageServicio) { }

  login(loginForm: NgForm): Observable<any> {
    return this.http.post(`${base_url}/login`, loginForm);
  }

  
  estaAutenticado(): boolean {
    this.userToken = this.tokenStorageServicio.leerToken();
    if (this.userToken.length < 1) {
      //Lo tengo true de momento
      return false;
    }else{
      return true;
    }
  }

   leerContenidoPublico(): Observable<any> {
    const url = `${base_url}/usuarios/test/all`
    return this.http.get(url, { responseType: 'text' });
  }


}
