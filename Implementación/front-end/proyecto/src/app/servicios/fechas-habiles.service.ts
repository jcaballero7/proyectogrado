import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { NgForm } from "@angular/forms";

import { environment } from 'src/environments/environment';
import { map } from "rxjs/operators";
import { Fechas_Habiles } from "../modelos/fechas-habiles.model";

const base_url = environment.base_url;

@Injectable()
export class FechasHabilesServicio {
    fechas_habiles: Observable<Fechas_Habiles[]>;
    fecha_habil: Observable<Fechas_Habiles>;

    constructor(private http: HttpClient,) { }

    leerFechas(): Observable<Fechas_Habiles[]> {
        const url = `${base_url}/fechas`;
        return this.http.get<Fechas_Habiles[]>(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    agregarFecha(fechasForm: NgForm) {
        const url = `${base_url}/fechas`;
        return this.http.post(url, fechasForm).pipe(map((resp: any) => {
            return resp;
        }));
    };

    leerFecha(id: number) {
        const url = `${base_url}/fechas/${id}`;
        return this.http.get(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    modificarFecha(id: number, fechasForm: NgForm) {
        const url = `${base_url}/fechas/${id}`;
        return this.http.put(url, fechasForm);
    }
}
