import { Injectable } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Observable } from "rxjs";

import { Proyecto } from "../modelos/proyecto.model";
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { Fechas_Habiles } from "../modelos/fechas-habiles.model";
import { Programa } from "../modelos/programa.model";
import { map } from "rxjs/operators";
const base_url = environment.base_url;


@Injectable()
export class ProyectoServicio {
    proyectos: Observable<Proyecto[]> | undefined;
    proyecto: Observable<Proyecto> | undefined;

    constructor(private http: HttpClient) { }


    leerProyectos(): Observable<Proyecto[]> {
        const url = `${base_url}/proyectos`;
        return this.http.get<Proyecto[]>(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    leerProyectosSinAval(): Observable<Proyecto[]> {
        const url = `${base_url}/proyectos/sinAval`;
        return this.http.get<Proyecto[]>(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    agregarProyecto(proyectoForm: NgForm) {
        const url = `${base_url}/proyectos`;
        return this.http.post(url, proyectoForm).pipe(map((resp: any) => {
        }));
    };

    leerProyecto(cod_proyecto: string) {
        const url = `${base_url}/proyectos/${cod_proyecto}`;
        return this.http.get(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    modificarProyecto(cod_proyecto: string, proyectoForm: NgForm) {
        const url = `${base_url}/proyectos/${cod_proyecto}`;
        return this.http.put(url, proyectoForm).pipe(map((resp: any) => {
        }));
    }

    leerDirectores(): Observable<String[]> {
        const url = `${base_url}/usuarios/directores`;
        return this.http.get<String[]>(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    leerEstudiantes(): Observable<String[]> {
        const url = `${base_url}/usuarios/estudiantes`;
        return this.http.get<String[]>(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    evaluarProyecto(cod_proyecto: any, aval: boolean) {
        const url = `${base_url}/proyectos/aprobar/${cod_proyecto}`;
        return this.http.put(url, {
            aval
        }).pipe(map((resp: any) => {
        }));
    }
}