import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { NgForm } from "@angular/forms";

import { environment } from 'src/environments/environment';
import { map } from "rxjs/operators";
import { Fechas_Habiles } from "../modelos/fechas-habiles.model";
import { Programa } from "../modelos/programa.model";

const base_url = environment.base_url;

@Injectable()
export class ProgramasServicio {
    programas: Observable<Programa[]>;
    programa: Observable<Programa>;

    constructor(private http: HttpClient,) { }

    leerProgramas(): Observable<Fechas_Habiles[]> {
        const url = `${base_url}/programas`;
        return this.http.get<Programa[]>(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    agregarPrograma(programaForm: NgForm) {
        const url = `${base_url}/programas`;
        return this.http.post(url, programaForm).pipe(map((resp: any) => {
            return resp;
        }));
    };

    leerPrograma(id: number) {
        const url = `${base_url}/programas/${id}`;
        return this.http.get(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    modificarPrograma(id: number, programaForm: NgForm) {
        const url = `${base_url}/programas/${id}`;
        return this.http.put(url, programaForm);
    }
}
