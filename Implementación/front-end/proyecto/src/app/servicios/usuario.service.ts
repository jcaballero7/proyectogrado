import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Usuario } from "../modelos/usuario.model";
import { NgForm, NgModel } from "@angular/forms";

import { environment } from 'src/environments/environment';
import { map } from "rxjs/operators";

const base_url = environment.base_url;

@Injectable()
export class UsuarioServicio {
    usuarios: Observable<Usuario[]>;
    usuario: Observable<Usuario>;
    roles: Observable<String[]>;

    constructor(private http: HttpClient) { }


    leerUsuarios(): Observable<Usuario[]> {
        const url = `${base_url}/usuarios`;
        return this.http.get<Usuario[]>(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    agregarUsuario(usuarioForm: NgForm) {
        const url = `${base_url}/usuarios`;
        return this.http.post(url, usuarioForm).pipe(map((resp: any) => {
            return resp;
        }));
    };

    leerUsuario(correo: string) {
        const url = `${base_url}/usuarios/${correo}`;
        return this.http.get(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    leerRoles(): Observable<String[]> {
        const url = `${base_url}/roles`;
        return this.http.get<String[]>(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    modificarUsuario(correo: string, usuarioForm: NgForm) {
        const url = `${base_url}/usuarios/${correo}`;
        return this.http.put(url, usuarioForm);
    }

    modificarContrasena(cambiarForm: NgModel) {
        const url = `${base_url}/usuarios/cambio`;
        return this.http.put(url, cambiarForm);
    }
}
