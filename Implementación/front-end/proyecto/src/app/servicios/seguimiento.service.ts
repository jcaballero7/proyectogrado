import { Injectable } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Observable } from "rxjs";
import { Seguimiento } from "../modelos/seguimiento.model";
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
const base_url = environment.base_url;

@Injectable()
export class SeguimientoServicio{
    seguimientos: Observable<Seguimiento[]>;
    seguimiento: Observable<Seguimiento>;

    constructor (private http: HttpClient){

    }

    leerSeguimientos(): Observable<Seguimiento[]> {
        return this.http.get<Seguimiento[]>(`${base_url}/seguimientos`).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    registrarSeguimiento(seguimientoForm: NgForm){
        const url = `${base_url}/seguimientos`;
        return this.http.post(url, seguimientoForm).pipe(map((resp: any) => {
            return resp;
        }));
    };

    leerSeguimiento(id_seguimiento: string){
        const url = `${base_url}/seguimientos/${id_seguimiento}`;
        return this.http.get(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    leerPendientes(codigo: any) {
        const url = `${base_url}/pendientes/${codigo}`;
        return this.http.get(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    leerHorasesoria(codigo: any) {
        const url = `${base_url}/pendientes/horario/${codigo}`;
        return this.http.get(url).pipe(
            map(resp => {
                return resp;
            })
        );
    }

    modificarSeguimiento(id_seguimiento: string, formData: FormData){
        const url = `${base_url}/seguimientos/${id_seguimiento}`;
        return this.http.put(url, formData);
    }
}