import { Injectable } from '@angular/core';

const TOKEN_KEY = 'x-token';
const USER_KEY = 'x-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageServicio {

  constructor() { }

  signOut(): void {
    window.sessionStorage.clear();
  }

  public guardarToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public leerToken(): string | null {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public guardarUsuario(usuario:any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(usuario));
  }

  public leerUsuario(): any {
    return JSON.parse(sessionStorage.getItem(USER_KEY)!);
  }
}