const { response } = require("express");
const jwt = require('jsonwebtoken');
const bcrypt = require("bcryptjs");

const db = require("../models");
const Usuario = db.usuarios;


const login = async (req, res = response) => {
  const { correo, contrasena } = req.body;


    Usuario.findByPk(correo)
      // Verificando si el usuario existe
      .then(usuarioDB => {
        if (!usuarioDB) {
          return res.status(404).json(
            msg = "El correo ingresado no se encuentra registrado.",
          );
        }
        // Verificando la contraseña
        const validContrasena = bcrypt.compareSync(contrasena, usuarioDB.contrasena);
        if (!validContrasena) {
          return res.status(401).json(
            msg = "Contraseña incorrecta."
          )
        }

        //Generar tocken -JWT
        const token = jwt.sign(
          { correo: correo },
          process.env.JWT_SECRET,
          {
            expiresIn: "12h",
          });


        const autorizaciones = [];
        usuarioDB.getRoles().then(roles => {
          for (let i = 0; i < roles.length; i++) {
            autorizaciones.push("ROLE_" + roles[i].rol.toUpperCase());
          }
          res.json({
            correo: usuarioDB.correo,
            nombre: usuarioDB.nombre,
            apellido: usuarioDB.apellido,
            roles: autorizaciones,
            token: token,
          });
        })
      })
      .catch(error => {
        res.status(500).json(
          msg = "Hable con el adminstrador. " + error,
        );
      });
};

module.exports = { login };