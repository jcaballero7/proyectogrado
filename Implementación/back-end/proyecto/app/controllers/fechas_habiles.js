const { response } = require("express");
const db = require("../models");
const Fechas_habiles = db.fechas_habiles;

const crearFechas = (req, res = response) => {
  Fechas_habiles.create(req.body)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send(
        msg = "Ha ocurrido un error al guardar. " + err
      );
    });
};

const consultarFechas = (req, res = response) => {
  Fechas_habiles.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send(
        msg = "Ha ocurrido un error al consultar las fechas_habiles. " + err
      );
    });
};

const consultarFecha = async (req, res = response) => {
  const { id } = req.params;
    try {
    const fechaDB = await Fechas_habiles.findByPk(id);
    res.json(
      fechaDB
    );
  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs. " + err,
    );
  };
}

const actualizarFechas = async (req, res = response) => {
  const { id } = req.params;

  try {
    const fechas_habiles = await Fechas_habiles.update(req.body, {
      where: { id: id }
    });
    res.json(
      msg ="Fechas actualizadas correctamente.",      
    );

  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs. " + err,
    );
  }
}

module.exports = { crearFechas, consultarFechas, consultarFecha, actualizarFechas }