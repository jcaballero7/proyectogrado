const { response } = require("express");
const generator = require('generate-password');
const bcrypt = require("bcryptjs");

const db = require("../models");
const { envioCorreoContrasena } = require("./correos");
const Usuario = db.usuarios;
const V_usuario = db.v_usuarios;
const Rol = db.roles;

const crearUsuario = async (req, res = response) => {

  const { correo } = req.body;

  const existeCorreo = await Usuario.findByPk(correo);

  if (existeCorreo) {
    return res.status(400).json(
      msg = "El correo ingresado ya está registrado."
    );
  }
  contrasena = generator.generate({
    length: 10,
    numbers: true
  });

  req.body.estado = "true";

  envioCorreoContrasena(req.body.correo, contrasena);

  // Encriptar contraseña
  const salt = bcrypt.genSaltSync();
  req.body.contrasena = bcrypt.hashSync(contrasena, salt);

  Usuario.create(req.body)
    .then(user => {
      const { rol } = req.body;

      if (req.body.rol) {
        Rol.findByPk(rol)
          .then(roles => {
            user.setRoles(roles).then(() => {
              res.send({
                msg: "Usuario creado.",
                //Quitar la siguiente linea, es solo para efectos de pruebas 
                user, contrasena, roles
              });
            });
          });
      } else {
        // user role = 1
        user.setRoles([1]).then(() => {
          res.send({
            msg: "Usuario creado.",
            //Quitar la siguiente linea, es solo para efectos de pruebas 
            user, contrasena, roles
          });
        });
      }
    })
    .catch(err => {
      res.status(500).json(
        msg = "Ha ocurrido un error al crear el usuario. " + err
      );
    });
};

const consultarUsuarios = (req, res = response) => {
  Usuario.findAll()
    .then(data => {
      res.json(
        data
      );
    })
    .catch(err => {
      res.status(400).json(
        msg = "Ha ocurrido un error al consultar los usuarios. " + err
      );
    });
};

const consultarEstudiantes = (req, res = response) => {
  V_usuario.findAll({
    attributes: ['correo'],
    where: { rol: "3" }
  })
    .then(data => {
      res.json(
        data
      );
    })
    .catch(err => {
      res.status(400).json(
        msg = "Ha ocurrido un error al consultar los estudiantes. " + err
      );
    });
};

const consultarDirectores = (req, res = response) => {
  V_usuario.findAll({
    attributes: ['correo'],
    where: { rol: "2" }
  })
    .then(data => {
      res.json(
        data
      );
    })
    .catch(err => {
      res.status(400).json(
        msg = "Ha ocurrido un error al consultar los directores. " + err
      );
    });
};


const consultarUsuario = async (req, res = response) => {
  const { correo } = req.params;
  try {
    const usuarioDB = await Usuario.findByPk(correo);

    //Consulta para cargar los roles en combo
    const usuarioRol = await V_usuario.findAll({
      attributes: ['rol'],
      where: { correo: correo }
    })

    if (usuarioRol[0].rol == 1) {
      usuarioRol[0].rol = "Administrador"
    } else if (usuarioRol[0].rol == 2) {
      usuarioRol[0].rol = "Director"
    } else {
      usuarioRol[0].rol = "Estudiante"
    }
    res.json({
      usuario: usuarioDB,
      rol: usuarioRol[0].rol
    });
  } catch (error) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs." + error,
    );
  };
}

const actualizarUsuario = async (req, res = response) => {
  const { correo } = req.params;
  if (req.body.rol == "Administrador") {
    req.body.rol = 1
  } else if (req.body.rol == "Director") {
    req.body.rol = 2
  } else {
    req.body.rol = 3
  }
  try {
    const usuario = await Usuario.update(req.body, {
      where: { correo: correo }
    });
    res.json(
      usuario
    );
  } catch (error) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs. " + error,
    );
  }
}

const actualizarContrasena = async (req, res = response) => {
  const { contrasena, con_nueva } = req.body;

  const usuarioDB = await Usuario.findByPk(req.userId);
  const validContrasena = bcrypt.compareSync(contrasena, usuarioDB.contrasena);
  if (!validContrasena) {
    return res.status(401).json(
      msg = "Contraseña incorrecta."
    )
  }

  // Encriptar contraseña
  const salt = bcrypt.genSaltSync();
  req.body.contrasena = bcrypt.hashSync(con_nueva, salt);

  Usuario.update(req.body,
    {
      where: { correo: req.userId }
    }).then(user => {
      res.json(
        msg = "Contraseña actualizada."
      )
    })
    .catch(err => {
      res.status(400).json(
        msg = "Ha ocurrido un error al actualizar la contraseña. " + err
      );
    });
}

module.exports = { crearUsuario, consultarUsuarios, consultarEstudiantes, consultarDirectores, consultarUsuario, actualizarUsuario, actualizarContrasena }