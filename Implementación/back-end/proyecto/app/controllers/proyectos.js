const db = require("../models");
const { response } = require("express");
const Sequelize = require('sequelize');
const Proyecto = db.proyectos;
const Usuario = db.usuarios;
const V_seguimiento = db.seguimientos;
const Op = Sequelize.Op

const crearProyecto = async (req, res = response) => {
  const { cod_proyecto } = req.body;
  // Por dejecto colocar los proyectos en estado activo
  req.body.estado = "true";

  // Se hace necesario recuperar el correo del objeto 
  req.body.cor_director = req.body.cor_director.correo;

  req.body.cor_estudiante1 = req.body.cor_estudiante1.correo;
  if (req.body.can_estudiantes > 1) {
    req.body.cor_estudiante2 = req.body.cor_estudiante2.correo;
    if (req.body.can_estudiantes > 2) {
      req.body.cor_estudiante3 = req.body.cor_estudiante3.correo;
    }
  }

  try {
    const existeProyecto = await Proyecto.findByPk(cod_proyecto);
    if (existeProyecto) {
      return res.status(400).json(
        msg = "El código de proyecto ingresado ya está registrado."
      );
    }
    const proyecto = await Proyecto.create(req.body);
    res.json({
      msg: "Proyecto creado correctamente.",
      proyecto
    });
  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... proyecto revisar logs." + err,
    );
  }
};

const consultarProyectos = async (req, res = response) => {

  Usuario.findByPk(req.userId).then(user => {
    user.getRoles().then(roles => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].rol === "Director") {
          Proyecto.findAll({
            where: {
              cor_director: req.userId
            }
          })
            .then(data => {
              res.json(
                data
              )
            })
            .catch(err => {
              res.status(400).json(
                msg = "Ha ocurrido un error al consultar los proyectos. " + err
              );
            });
        } else {
          Proyecto.findAll()
            .then(data => {
              res.json(
                data
              );
            })
            .catch(err => {
              res.status(400).json(
                msg = "Ha ocurrido un error al consultar los proyectos. " + err
              );
            });
        }
      }
    });
  });
};

const consultarProyectosSinAval = async (req, res = response) => {

  Usuario.findByPk(req.userId).then(user => {
    user.getRoles().then(roles => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].rol === "Director") {
          Proyecto.findAll({
            where: {
              cor_director: req.userId, aval: null
            }
          })
            .then(data => {
              res.json(
                data
              )
            })
            .catch(err => {
              res.status(400).json(
                msg = "Ha ocurrido un error al consultar los proyectos. " + err
              );
            });
        } else if (roles[i].rol === "Estudiante") {
          Proyecto.findAll({
            where: {
              [Op.or]: [{cor_estudiante1: req.userId}, {cor_estudiante2: req.userId}, {cor_estudiante3: req.userId}], aval: null
            }
          })
            .then(data => {
              res.json(
                data
              )
            })
            .catch(err => {
              res.status(400).json(
                msg = "Ha ocurrido un error al consultar los proyectos. " + err
              );
            });
        } else {
          Proyecto.findAll({
            where: {
              aval: null
            }
          })
            .then(data => {
              res.json(
                data
              );
            })
            .catch(err => {
              res.status(400).json(
                msg = "Ha ocurrido un error al consultar los proyectos. " + err
              );
            });
        }
      }
    });
  });
};

const consultarProyecto = async (req, res = response) => {
  const { cod_proyecto } = req.params;
  try {
    const proyectoDB = await Proyecto.findByPk(cod_proyecto);
    res.json(
      proyectoDB
    );
  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs. " + err,
    );
  };
}

const actualizarProyecto = async (req, res = response) => {
  const { cod_proyecto } = req.params;

  if (!req.body.cor_director.correo) {
  } else {
    req.body.cor_director = req.body.cor_director.correo
  }
  try {
    const proyecto = await Proyecto.update(req.body, {
      where: { cod_proyecto: cod_proyecto }
    });
    res.json(
      msg = "Proyecto actualizado correctamente.",
    );

  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs. " + err,
    );
  }
}

const evaluarProyecto = async (req, res = response) => {
  const { cod_proyecto, } = req.params;

  try {
    Proyecto.update(req.body, {
      where: { cod_proyecto: cod_proyecto }
    })
    res.json(
      msg = "Proyecto evaluado."
    );
  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs. " + err,
    );
  };
}



module.exports = { crearProyecto, consultarProyectos, consultarProyecto, actualizarProyecto, evaluarProyecto, consultarProyectosSinAval }