const db = require("../models");
const { response } = require("express");
const V_seguimiento = db.v_seguimientos;


const consultarPendientes = (req, res = response) => {

  const { codigo } = req.params;

  V_seguimiento.findAll({
    attributes: ['pendientes'],
    /*limit:1,*/
    where: {
      cod_proyecto: codigo
    }
  })
    .then(data => {
      res.json(
        data
      );
    })
    .catch(err => {
      res.status(400).json(
        msg = "Ha ocurrido un error al consultar los pendientes. " + err
      );
    });
};

const consultarHorasesoria = (req, res = response) => {

  const { codigo } = req.params;

  V_seguimiento.findAll({
    attributes: ['hor_asesoria'],
    /*limit:1,*/
    where: {
      cod_proyecto: codigo
    }
  })
    .then(data => {
      res.json(
        data
      );
    })
    .catch(err => {
      res.status(400).json(
        msg = "Ha ocurrido un error al consultar el horario de asesoria. " + err
      );
    });
};


module.exports = { consultarPendientes, consultarHorasesoria }