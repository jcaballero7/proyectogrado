const db = require("../models");
const { response } = require("express");
const Programa = db.programas;

const crearPrograma = async (req, res = response) => {

  try {
    Programa.create(req.body);
    res.json(
      msg = "Programa creado correctamente.",
    );
  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... programa revisar logs." + err,
    );
  }
};

const consultarProgramas = (req, res = response) => {

  Programa.findAll(
    {
    //attributes: ['nombre'],
    }
  )
    .then(data => {
      res.json(
        data
      );
    })
    .catch(err => {
      res.status(400).json(
        msg = "Ha ocurrido un error al consultar los progamas. " + err
      );
    });
}
 
const consultarPrograma = async (req, res = response) => {
  const { id } = req.params;
  try {
    const programaDB = await Programa.findByPk(id);
    res.json(
      programaDB
    );
  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... consulta de programa, revisar logs. " + err,
    );
  };
}

const actualizarPrograma = async (req, res = response) => {
  const { id } = req.params;
  
  try {
    Programa.update(req.body, {
      where: { id: id }
    });
    res.json(
      msg = "Programa actualizado correctamente.",
    );

  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... actualizando programa, revisar logs. " + err,
    );
  }
}

module.exports = { crearPrograma, consultarPrograma, consultarProgramas, actualizarPrograma }