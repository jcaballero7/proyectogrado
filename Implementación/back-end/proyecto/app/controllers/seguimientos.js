const db = require("../models");
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const { response } = require("express");
const { envioCorreoSeguimiento, envioCorreoAprobarSeguimiento } = require("./correos");

const Seguimiento = db.seguimientos;
const Usuario = db.usuarios;
const Anexo = db.anexos;
const Proyecto = db.proyectos;
const V_usuario = db.v_usuarios;

const fs = require('fs-extra');

const crearSeguimiento = async (req, res = response) => {
  try {
    const seguimiento = await Seguimiento.create(req.body);

    //Envio de correo al administrador
    const adminsitrador = await V_usuario.findAll({
      attributes: ['correo'],
      where: { rol: "1" }
    })

    JSON.stringify(adminsitrador[0].correo);

    const proyecto = await Proyecto.findByPk(seguimiento.cod_proyecto);
    const can_estudiantes = proyecto.can_estudiantes;

    if (can_estudiantes == 1){

      envioCorreoAprobarSeguimiento(seguimiento.cor_estudiante1, seguimiento.cod_proyecto, seguimiento.hor_asesoria);

    }
    else if (can_estudiantes == 2){
      
      envioCorreoAprobarSeguimiento(seguimiento.cor_estudiante1, seguimiento.cod_proyecto, seguimiento.hor_asesoria);
      envioCorreoAprobarSeguimiento(seguimiento.cor_estudiante2, seguimiento.cod_proyecto, seguimiento.hor_asesoria);

    }
    else{

      envioCorreoAprobarSeguimiento(seguimiento.cor_estudiante1, seguimiento.cod_proyecto, seguimiento.hor_asesoria);
      envioCorreoAprobarSeguimiento(seguimiento.cor_estudiante2, seguimiento.cod_proyecto, seguimiento.hor_asesoria);
      envioCorreoAprobarSeguimiento(seguimiento.cor_estudiante3, seguimiento.cod_proyecto, seguimiento.hor_asesoria);
    }

    envioCorreoSeguimiento(adminsitrador[0].correo, seguimiento.cod_proyecto, seguimiento.hor_asesoria);


    res.json(
      msg = "Seguimiento registrado correctamente.",
    );

  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... seguimientos revisar logs." + err,
    );
  }
};

const consultarSeguimientos = (req, res = response) => {

  Usuario.findByPk(req.userId).then(user => {
    user.getRoles().then(roles => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].rol === "Director") {
          Seguimiento.findAll({
            where: {
              cor_director: req.userId
            },
            order: [
              ['id_seguimiento', 'DESC'],
            ],
          })
            .then(data => {
              res.json(
                data
              );
            })
            .catch(err => {
              res.status(400).json(
                msg = "Ha ocurrido un error al consultar los seguimientos. " + err
              );
            });
        } else if (roles[i].rol === "Estudiante") {
          Seguimiento.findAll({
            where: {
              [Op.or]: [{cor_estudiante1: req.userId}, {cor_estudiante2: req.userId}, {cor_estudiante3: req.userId}]
            },
            order: [
              ['id_seguimiento', 'DESC'],
            ],
          })
            .then(data => {
              res.json(
                data
              );
            })
            .catch(err => {
              res.status(400).json(
                msg = "Ha ocurrido un error al consultar los seguimientos. " + err
              );
            });
        } 
        
        else {
          Seguimiento.findAll({
            order: [
              ['id_seguimiento', 'DESC'],
            ],
          }
          )
            .then(data => {
              res.json(
                data
              );
            })
            .catch(err => {
              res.status(400).json(
                msg = "Ha ocurrido un error al consultar los seguimientos. " + err
              );
            });
        }
      }
    });
  });
};

const consultarSeguimiento = async (req, res = response) => {
  const { id_seguimiento } = req.params;
  try {
    const seguimientoDB = await Seguimiento.findByPk(id_seguimiento);

    const anexo = await Anexo.findAll({
      where: { id_seguimiento: id_seguimiento }
    });

    res.json({
      seguimiento: seguimientoDB,
      anexo
    })

  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs. " + err,
    );
  };
}


const actualizarSeguimiento = async (req, res = response) => {
  const { id_seguimiento } = req.params;

  //Se consulta el código del proyecto  
  const codigo = await Seguimiento.findByPk(id_seguimiento);


  //const dir = "D:\\PG2\\proyectogrado\\Implementación\\back-end\\" + codigo.cod_proyecto + "\\" + id_seguimiento;
  const dir = "D:\\Proyectogrado\\proyectogrado\\Implementación\\back-end\\proyecto\\archivos\\" + codigo.cod_proyecto + "\\" + id_seguimiento;
  //Crea una carpeta para cada proyecto y id de seguimiento
  fs.ensureDirSync(dir);

  if (req.files) {


    if ((Object(req.files.archivo)).length > 1) {
      for (let file of req.files.archivo) {
        JSON.stringify(file.mv(dir + "\\" + file.name));
        req.body.id_seguimiento = id_seguimiento;
        req.body.anexo = file.name;
        await Anexo.create(req.body);
      }
    } else {
      JSON.stringify(req.files.archivo.mv(dir + "\\" + req.files.archivo.name));
      req.body.id_seguimiento = id_seguimiento;
      req.body.anexo = req.files.archivo.name;
      await Anexo.create(req.body);
    }

  };


  try {
    Seguimiento.update(req.body, {
      where: { id_seguimiento: id_seguimiento }
    });
    res.json(
      msg = "Seguimiento actualizado correctamente.",
    );

  } catch (err) {
    res.status(500).json(
      msg = "Error inesperado... revisar logs. " + err,
    );
  }
}

module.exports = { crearSeguimiento, consultarSeguimientos, consultarSeguimiento, actualizarSeguimiento }