const db = require("../models");
const { response } = require("express");
const Rol = db.roles;


const consultarRoles = (req, res = response) => {

  Rol.findAll()
    .then(data => {
      res.json(
        data
      );
    })
    .catch(err => {
      res.status(400).json(
        msg = "Ha ocurrido un error al consultar los roles. " + err
      );
    });
};


module.exports = { consultarRoles }