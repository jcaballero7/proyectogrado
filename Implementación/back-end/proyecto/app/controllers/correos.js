const nodeMailer = require('nodemailer');

const config = nodeMailer.createTransport({

    host: 'smtp.gmail.com',
    post: 587,
    secure: false,
    requireTLS: true,
    auth: {
        user: 'celisadni@gmail.com',
        pass: 'adni--2196'
    }
});

const envioCorreoContrasena = (correo, contrasena) => {

    const bodyc = correo;
    const bodyp = contrasena;

    const opciones = {
        from: 'Contraseña',
        subject: 'Contraseña generada.',
        to: bodyc,
        text: 'La contraseña para el acceso al seguimiento del proyecto de grado es: ' + bodyp
    };

    config.sendMail(opciones, function (res = response, error, result) {

        if (error) return res.json({
            ok: false,
            msg: error
        });
        return res.json(
            msg = result
        );

    });

}

const envioCorreoSeguimiento = (correo, proyecto, fecha) => {

    const bodyc = correo;
    const bodyp = proyecto;

    const opciones = {
        from: 'Seguimiento',
        subject: 'Seguimiento generado.',
        to: bodyc,
        text: 'Se ha generado el seguimiento para el proyecto: ' + bodyp + ' correspondiente al encuentro de la fecha: '+ fecha
    };

    config.sendMail(opciones, function (res = response, error, result) {

        if (error) return res.json({
            ok: false,
            msg: error
        });
        return res.json(
            msg = result
        );
    });

}

const envioCorreoAprobarSeguimiento = (correo, proyecto, fecha) => {

    const bodyc = correo;
    const bodyp = proyecto;
    const hor_asesoria = fecha;

    const opciones = {
        from: 'Aprobar seguimiento',
        subject: 'Seguimiento generado.',
        to: bodyc,
        text: 'Se ha generado el seguimiento para el proyecto: ' + bodyp + ' correspondiente al encuentro de la fecha: '+ hor_asesoria + ' por favor aprobar.' 
    };

    config.sendMail(opciones, function (res = response, error, result) {

        if (error) return res.json({
            ok: false,
            msg: error
        });
        return res.json(
            msg = result
        );
    });

}

module.exports = {
    envioCorreoContrasena, envioCorreoSeguimiento, envioCorreoAprobarSeguimiento
}