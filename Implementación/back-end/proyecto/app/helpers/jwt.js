const jwt = require('jsonwebtoken');

const generarJWT = (correo) => {
  return new Promise((resolve, reject) => {
    const payload = {
      correo,
    };

    jwt.sign(
      payload,
      process.env.JWT_SECRET,
      {
        expiresIn: "1h",
      },
      (err, token) => {
        if (err) {
          reject("No se pudo generar el JWT")
        }else{
            resolve (token);
        }
      }
    );
  });
};


module.exports = {
    generarJWT,
}