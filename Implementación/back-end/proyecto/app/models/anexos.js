module.exports = (sequelize, Sequelize) => {
  const Anexos = sequelize.define("anexos", {
    id_anexo: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,      
      require: true,      
    },
    id_seguimiento: {
      type: Sequelize.INTEGER,
      require: true,
    },
    anexo: {
      type: Sequelize.STRING,
      require: true,
    },
  },
  {
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  
    // If don't want createdAt
    createdAt: false,
  
    // If don't want updatedAt
    updatedAt: false,
  
    // your other configuration here
  
  });
  return Anexos;
};
