module.exports = (sequelize, Sequelize) => {
    const Proyectos = sequelize.define("proyectos", {
      cod_proyecto: {
        type: Sequelize.STRING,
        primaryKey: true,
        require: true,
      },
      titulo: {
        type: Sequelize.STRING,
        require: false,
      },
        programa: {
        type: Sequelize.STRING,
        require: true,
      },
      hor_asesoria: {
        type: Sequelize.STRING,
        require: true
      },
      cor_director: {
        type: Sequelize.STRING,
        require: true
      },
      can_estudiantes: {
        type: Sequelize.INTEGER,
        require: true
      },
      cor_estudiante1: {
        type: Sequelize.STRING,
        require: true
      },
      cor_estudiante2: {
        type: Sequelize.STRING,
        require: false
      },
      cor_estudiante3: {
        type: Sequelize.STRING,
        require: false
      },
      tip_proyecto: {
        type: Sequelize.STRING,
        require: true
      },
      estado: {
        type: Sequelize.BOOLEAN,
        require: true
      },
      aval: {
        type: Sequelize.BOOLEAN
      }
    },
    {
      // don't add the timestamp attributes (updatedAt, createdAt)
      timestamps: false,
    
      // If don't want createdAt
      createdAt: false,
    
      // If don't want updatedAt
      updatedAt: false,
    
      // your other configuration here
    
    });
    Proyectos.removeAttribute('id');
    return Proyectos;
  };
  