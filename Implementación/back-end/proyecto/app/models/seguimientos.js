module.exports = (sequelize, Sequelize) => {
    const Seguimientos = sequelize.define("seguimientos", {
      id_seguimiento: {
        type: Sequelize.STRING,
        primaryKey: true,
      },
      cod_proyecto: {
        type: Sequelize.STRING,
        require: false,
      },
      hor_asesoria: {
        type: Sequelize.STRING,
        require: true,
      },
      pendientes: {
        type: Sequelize.STRING,
        require: false
      },
      des_tematica: {
        type: Sequelize.STRING,
        require: true
      },
      hallazgos: {
        type: Sequelize.STRING,
        require: true
      },
      cor_director: {
        type: Sequelize.STRING,
        require: true
      },
      ace_director: {
        type: Sequelize.BOOLEAN,
        require: false
      },
      cor_estudiante1: {
        type: Sequelize.STRING,
        require: true
      },
      ace_estudiante1: {
        type: Sequelize.BOOLEAN,
        require: false
      },
      cor_estudiante2: {
        type: Sequelize.STRING,
        require: false
      },
      ace_estudiante2: {
        type: Sequelize.BOOLEAN,
        require: false
      },
      cor_estudiante3: {
        type: Sequelize.STRING,
        require: false
      },
      ace_estudiante3: {
        type: Sequelize.BOOLEAN,
        require: false
      },
      asi_estudiante1: {
        type: Sequelize.BOOLEAN,
        require: false
      },
      asi_estudiante2: {
        type: Sequelize.BOOLEAN,
        require: false
      },
      asi_estudiante3: {
        type: Sequelize.BOOLEAN,
        require: false
      },
      com_estudiante1: {
        type: Sequelize.STRING,
        require: false
      },
      com_estudiante2: {
        type: Sequelize.STRING,
        require: false
      },
      com_estudiante3: {
        type: Sequelize.STRING,
        require: false
      },      
      pro_evaluado: {
        type: Sequelize.BOOLEAN        
      },
      asi_director: {
        type: Sequelize.STRING,
        require: true
      },  
    },
    {
      // don't add the timestamp attributes (updatedAt, createdAt)
      timestamps: false,
    
      // If don't want createdAt
      createdAt: false,
    
      // If don't want updatedAt
      updatedAt: false,
    
      // your other configuration here
    
    });
    Seguimientos.removeAttribute('id');
    return Seguimientos;
  };
  