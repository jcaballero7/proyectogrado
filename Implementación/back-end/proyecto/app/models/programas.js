module.exports = (sequelize, Sequelize) => {
  const Programas = sequelize.define("programas", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,      
      require: true,      
    },
    nombre: {
      type: Sequelize.STRING,
      require: true,
    },
  },
  {
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  
    // If don't want createdAt
    createdAt: false,
  
    // If don't want updatedAt
    updatedAt: false,
  
    // your other configuration here
  
  });
  return Programas;
};
