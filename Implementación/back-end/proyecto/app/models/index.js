const dbConfig = require("../config/db.config");
const Sequelize = require("sequelize", 'sequelize-views-support');
const express = require('express');

const sequelize = new Sequelize(
  dbConfig.DB,
  dbConfig.USER,
  dbConfig.PASSWORD, 
  {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.fechas_habiles = require("./fechas_habiles")(sequelize, Sequelize);

db.usuarios = require("./usuarios")(sequelize, Sequelize);

db.roles = require("./roles")(sequelize, Sequelize);

db.proyectos = require("./proyectos")(sequelize, Sequelize);
db.programas = require("./programas")(sequelize, Sequelize);

db.seguimientos = require("./seguimientos")(sequelize, Sequelize);

db.v_usuarios = require("./v_usuarios")(sequelize, Sequelize);

db.v_seguimientos = require("./v_seguimientos")(sequelize, Sequelize);

db.anexos = require("./anexos")(sequelize, Sequelize);


db.roles.belongsToMany(db.usuarios, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});

db.usuarios.belongsToMany(db.roles, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId"
});

db.ROLES = ["Administrador", "Director", "Estudiante"];

module.exports = db;