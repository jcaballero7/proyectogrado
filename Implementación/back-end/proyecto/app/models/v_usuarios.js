module.exports = (sequelize, Sequelize) => {
    const V_usuarios = sequelize.define("v_usuarios", {
        correo: Sequelize.STRING,
        rol: Sequelize.STRING,            
    },
        {
            treatAsView: true,
            viewDefinition: `CREATE OR REPLACE VIEW V_USUARIOS AS
            SELECT s.correo
            ,	   r.id
            FROM   roles        r 
            ,	   user_roles	u
            ,      usuarios		s
            WHERE  r.id     = u.roleid
            and	   u.userid = s.correo;`
    });
    return V_usuarios;
  };
  