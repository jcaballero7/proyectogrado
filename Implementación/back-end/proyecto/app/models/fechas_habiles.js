module.exports = (sequelize, Sequelize) => {
  const Fechas_habiles = sequelize.define("fechas_habiles", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,   
      require: true,      
    },
    fec_ini: {
      type: Sequelize.STRING,
      require: true,
    },
      fec_fin: {
      type: Sequelize.STRING,
      require: true,
    },
    descripcion: {
      type: Sequelize.STRING,
      require: true
    }
  },
  {
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  
    // If don't want createdAt
    createdAt: false,
  
    // If don't want updatedAt
    updatedAt: false,
  
    // your other configuration here
  
  });
  return Fechas_habiles;
};
