module.exports = (sequelize, Sequelize) => {
    const Roles = sequelize.define("roles", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      rol: {
        type: Sequelize.STRING,
        require: true,
      }
    },
    {
  
      // don't add the timestamp attributes (updatedAt, createdAt)
      timestamps: false,
    
      // If don't want createdAt
      createdAt: false,
    
      // If don't want updatedAt
      updatedAt: false,
    
      // your other configuration here
    
    });
    return Roles;
  };
  