module.exports = (sequelize, Sequelize) => {
    const Usuarios = sequelize.define("usuarios", {
      correo: {
        type: Sequelize.STRING,
        primaryKey: true,
        require: true,
      },
      contrasena: {
        type: Sequelize.STRING,
        require: true,
      },
        nombre: {
        type: Sequelize.STRING,
        require: true,
      },
      apellido: {
        type: Sequelize.STRING,
        require: true
      },
      tip_documento: {
        type: Sequelize.STRING,
        require: true,
      },
      num_documento: {
        type: Sequelize.DECIMAL,
        require: true
      },
      telefono: {
        type: Sequelize.DECIMAL,
        require: true
      },
      // rol: {
      //   type: Sequelize.DECIMAL,
      //   require: true
      // },
      estado: {
        type: Sequelize.BOOLEAN,
        require: true
      }
    },
    {
      // don't add the timestamp attributes (updatedAt, createdAt)
      timestamps: false,
    
      // If don't want createdAt
      createdAt: false,
    
      // If don't want updatedAt
      updatedAt: false,
    
      // your other configuration here
    
    });
    Usuarios.removeAttribute('id');
    return Usuarios;
  };
  