module.exports = (sequelize, Sequelize) => {
    const V_seguimientos = sequelize.define("v_seguimientos", {
        cod_proyecto: Sequelize.STRING,
        pendientes: Sequelize.STRING, 
        hor_asesoria: Sequelize.STRING,              
    },
        {
            treatAsView: true,
            viewDefinition: ``
    });
    return V_seguimientos;
  };
  