const jwt = require("jsonwebtoken");
const db = require("../models");
const Usuario = db.usuarios;

const validarJWT = (req, res, next) => {
  //Leer el token
  const token = req.header("x-token");

  if (!token) {
    return res.status(403).json({
      msg: "No hay un token en la petición."});
    }

    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err){
        return res.status(401).json({
          msg: "Token invalido",
        });
      }
      req.userId = decoded.correo;
    next();
    });
};


module.exports = {
  validarJWT,
};
