/*
    Ruta: /api/proyectos
*/

const { Router } = require("express");
const { crearProyecto, consultarProyectos, 
    consultarProyecto, actualizarProyecto, 
    evaluarProyecto,consultarProyectosSinAval } = require("../controllers/proyectos");
const { validarJWT } = require("../middleware/validar-jwt");
const { check } = require("express-validator");
const { validarCampos } = require("../middleware/validar-campos");
const router = Router();

// Crear proyecto
router.post("/", [validarJWT,
    check("cod_proyecto", "El cod_proyecto es obligatorio.").not().isEmpty(),
    check("titulo", "El titulo es obligatorio.").not().isEmpty(),
    check("programa", "El programa es obligatorio.").not().isEmpty(),
    check("hor_asesoria", "El hor_asesoria es obligatorio.").not().isEmpty(),
    check("cor_director", "El cor_director es obligatorio.").not().isEmpty(),
    check("can_estudiantes", "El can_estudiantes es obligatorio.").not().isEmpty(),
    check("cor_estudiante1", "El cor_estudiante1 es obligatorio.").not().isEmpty(),
    check("tip_proyecto", "El tip_proyecto es obligatorio.").not().isEmpty(),
    //check("estado", "El estado es obligatorio").not().isEmpty(),
    validarCampos
],
    crearProyecto);

// Consultar proyectos
router.get("/", [validarJWT,], consultarProyectos);
router.get("/sinAval", [validarJWT,], consultarProyectosSinAval);


// Consultar proyecto por código
router.get("/:cod_proyecto", [validarJWT,
    check("cod_proyecto", "El código de proyecto es obligatorio."),
    validarCampos
],
    consultarProyecto);

// Actualizar-editar proyecto
router.put("/:cod_proyecto", [validarJWT,
    check("cod_proyecto", "El código de proyecto es obligatorio."),
    validarCampos
],
    actualizarProyecto);

// Aprobar proyecto
router.put("/aprobar/:cod_proyecto", [validarJWT,
    check("cod_proyecto", "El código de proyecto es obligatorio."),
    validarCampos
],
    evaluarProyecto);    

module.exports = router;