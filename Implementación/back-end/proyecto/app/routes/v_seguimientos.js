/*
    Ruta: /api/pendientes
*/

const {Router} = require("express");
const {consultarPendientes, consultarHorasesoria } = require("../controllers/v_seguimientos");
const { validarJWT } = require("../middleware/validar-jwt");
const router = Router();

// Consultar Pendientes
router.get("/:codigo", [validarJWT], consultarPendientes);

// Consultar Horario Asesoria
router.get("/horario/:codigo", [validarJWT], consultarHorasesoria);

module.exports = router;