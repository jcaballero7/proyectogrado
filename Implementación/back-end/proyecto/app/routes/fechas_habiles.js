/*
    Ruta: /api/fechas_habiles
*/

const { Router } = require("express");
const { crearFechas, consultarFechas, consultarFecha, actualizarFechas } = require("../controllers/fechas_habiles");
const { validarCampos } = require("../middleware/validar-campos");
const { check } = require("express-validator");
const { validarJWT } = require("../middleware/validar-jwt");

const router = Router();

// Crear fechas
router.post("/", [validarJWT,
    check("fec_ini", "La fecha inicial es obligatoria.").not().isEmpty(),
    check("fec_fin", "La fecha final es obligatoria.").not().isEmpty(),
    check("descripcion", "La descripcion es obligatoria.").not().isEmpty(),
    validarCampos,
     ], 
     crearFechas);

// Consultar fechas
router.get("/", [validarJWT], consultarFechas);

// Consultar fecha
router.get("/:id", [validarJWT,
    check("id", "El id es obligatorio.").not().isEmpty(),
    validarCampos
], consultarFecha);

// Actualizar fecha
router.put("/:id", [validarJWT,
    check("id", "El id de la fecha es obligatorio.").not().isEmpty(),
    validarCampos
], actualizarFechas);

module.exports = router;