/*
    Ruta: /api/login
*/

const { Router } = require("express");
const { check } = require("express-validator");
const router = Router();
const { login,  renewToken } = require("../controllers/login");
const { validarCampos } = require("../middleware/validar-campos");
//const { validarJWT } = require("../middleware/validar-jwt");

router.post("/", [
    check("correo", "El correo es obligatorio").isEmail(),
    check("contrasena", "La contraseña es obligatoria").not().isEmail(),
    validarCampos
], login);


// router.get("/renew",
//     validarJWT, renewToken);

module.exports = router;
