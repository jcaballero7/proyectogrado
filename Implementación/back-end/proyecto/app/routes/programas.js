/*
    Ruta: /api/programas
*/

const { Router } = require("express");
const { validarCampos } = require("../middleware/validar-campos");
const { check } = require("express-validator");
const { validarJWT } = require("../middleware/validar-jwt");
const { crearPrograma, consultarProgramas, consultarPrograma, actualizarPrograma } = require("../controllers/programas");

const router = Router();

// Crear programa
router.post("/", [validarJWT,
    check("nombre", "El nombre es obligatoria.").not().isEmpty(),
    validarCampos,
     ], 
     crearPrograma);

// Consultar programas
router.get("/", [validarJWT], consultarProgramas);

// Consultar programa
router.get("/:id", [validarJWT,
    check("id", "El id es obligatorio.").not().isEmpty(),
    validarCampos
], consultarPrograma);

// Actualizar programa
router.put("/:id", [validarJWT,
    check("id", "El id es obligatorio.").not().isEmpty(),
    validarCampos
], actualizarPrograma);

module.exports = router;