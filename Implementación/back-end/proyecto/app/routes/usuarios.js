/*
    Ruta: /api/fechas_habiles
*/

const { Router } = require("express");
const { check } = require("express-validator");
const { crearUsuario, consultarUsuarios, consultarEstudiantes, consultarDirectores, consultarUsuario, actualizarUsuario, actualizarContrasena } = require("../controllers/usuarios");
const { validarCampos } = require("../middleware/validar-campos");
const { validarJWT } = require("../middleware/validar-jwt");

const router = Router();

// Crear usuario
router.post("/", [validarJWT,
    check("correo", "El correo es obligatorio.").isEmail(),
    check("nombre", "El nombre es obligatorio.").not().isEmpty(),
    check("apellido", "El apellido es obligatorio.").not().isEmpty(),
    check("tip_documento", "El tipo de documento es obligatorio.").not().isEmpty(),
    check("num_documento", "El numero de documento es obligatorio.").not().isEmpty(),
    check("telefono", "El telefono es obligatorio.").not().isEmpty(),
    //check("rol", "El rol es obligatorio").not().isEmpty(),
    validarCampos
], crearUsuario);

// Consultar usuarios
router.get("/", [validarJWT,
    consultarUsuarios]);

router.get("/estudiantes", [validarJWT], consultarEstudiantes);

router.get("/directores", [validarJWT], consultarDirectores);

// Consultar usuario por correo
router.get("/:correo", [validarJWT,
    check("correo", "El correo es obligatorio.").not().isEmpty(),
    validarCampos
], consultarUsuario);

// Actualizar contraseña usuario
router.put("/cambio", [validarJWT,
    check("contrasena", "La contraseña es obligatoria.").not().isEmpty(),
    check("con_nueva", "La contraseña nueva es obligatoria.").not().isEmpty(),
    validarCampos
], actualizarContrasena);

// Actualizar-editar usuario
router.put("/:correo", [validarJWT,
    check("correo", "El correo es obligatorio.").not().isEmpty(),
    validarCampos
], actualizarUsuario);



module.exports = router;