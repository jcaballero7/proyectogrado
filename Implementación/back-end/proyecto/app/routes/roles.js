/*
    Ruta: /api/roles
*/

const {Router} = require("express");
const {consultarRoles } = require("../controllers/roles");
const { validarJWT } = require("../middleware/validar-jwt");
const router = Router();

  // Consultar roles
router.get("/", [validarJWT], consultarRoles);

module.exports = router;