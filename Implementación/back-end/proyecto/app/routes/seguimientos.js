/*
    Ruta: /api/seguimientos
*/

const { Router } = require("express");
const { crearSeguimiento, consultarSeguimientos, consultarSeguimiento, actualizarSeguimiento } = require("../controllers/seguimientos");
const { validarJWT } = require("../middleware/validar-jwt");
const router = Router();

// Crear seguimiento
router.post("/", [], crearSeguimiento);

// Consultar seguimientos
router.get("/", [validarJWT], consultarSeguimientos);

// Consultar seguimiento por código
router.get("/:id_seguimiento", [validarJWT], consultarSeguimiento);

// Actualizar-editar seguimiento
router.put("/:id_seguimiento", [validarJWT, actualizarSeguimiento]);

module.exports = router;