require("dotenv").config();
const PORT = process.env.PORT;

const express = require("express");
const cors = require("cors");
const fileUpload = require('express-fileupload');
const path = require('path');

const app = express();

app.use(fileUpload({}));


var corsOptions = {
  origin: "http://localhost:4200"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json({})); 
//

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));   

const db = require("./app/models");
const Roles = db.roles;


db.sequelize.sync();
// // drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });

// db.sequelize.sync({force: true}).then(() => {
//   console.log('Drop and Resync Db');
initial();
// });

function initial() {
  Roles.create({
    id: 1,
    rol: "Administrador"
  });
 
  Roles.create({
    id: 2,
    rol: "Director"
  });
 
  Roles.create({
    id: 3,
    rol: "Estudiante"
  });
}

// Ruta simple de bienvenida
// app.get("/", (req, res) => {
//   res.json({ message: "Welcome to proyect application." });
// });

// Ruta para login
app.use("/api/login", require("./app/routes/login"));

// Ruta para fechas habiles
app.use("/api/fechas/", require ("./app/routes/fechas_habiles"));

// Ruta para los programas
app.use("/api/programas/", require ("./app/routes/programas"));

// Ruta para los usuarios
app.use("/api/usuarios/", require ("./app/routes/usuarios"));

// Ruta para los roles
app.use("/api/roles/", require ("./app/routes/roles"));

// Ruta para los proyectos
app.use("/api/proyectos/", require ("./app/routes/proyectos"));

// Ruta para los seguimientos
app.use("/api/seguimientos/", require ("./app/routes/seguimientos"));

// Ruta para los pendientes
app.use("/api/pendientes/", require ("./app/routes/v_seguimientos"));

// función middleware para servir archivos estáticos
app.use(express.static(path.join(__dirname, 'archivos')));

// set port, listen for requests
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
