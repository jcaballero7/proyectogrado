CREATE DATABASE  IF NOT EXISTS `proyecto` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `proyecto`;
-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: proyecto
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anexos`
--

DROP TABLE IF EXISTS `anexos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `anexos` (
  `id_anexo` int NOT NULL AUTO_INCREMENT COMMENT 'Identificador único del anexo',
  `anexo` varchar(250) DEFAULT NULL COMMENT 'Descripción del anexo',
  `id_seguimiento` int NOT NULL COMMENT 'Identificador único de donde se genera el anexo',
  PRIMARY KEY (`id_anexo`),
  KEY `id_seguimiento` (`id_seguimiento`),
  CONSTRAINT `anexos_ibfk_1` FOREIGN KEY (`id_seguimiento`) REFERENCES `seguimientos` (`id_seguimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla para el registro anexos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anexos`
--

LOCK TABLES `anexos` WRITE;
/*!40000 ALTER TABLE `anexos` DISABLE KEYS */;
/*!40000 ALTER TABLE `anexos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auditoria` (
  `tabla` varchar(30) NOT NULL,
  `campo` varchar(50) DEFAULT NULL,
  `accion` varchar(50) DEFAULT NULL,
  `llave` varchar(80) DEFAULT NULL,
  `val_anterior` varchar(200) DEFAULT NULL,
  `val_actual` varchar(200) DEFAULT NULL,
  `observacion` varchar(200) DEFAULT NULL,
  `fec_sistema` date NOT NULL,
  `usu_sistema` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria`
--

LOCK TABLES `auditoria` WRITE;
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fechas_habiles`
--

DROP TABLE IF EXISTS `fechas_habiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fechas_habiles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fec_ini` varchar(50) NOT NULL,
  `fec_fin` varchar(50) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fechas_habiles`
--

LOCK TABLES `fechas_habiles` WRITE;
/*!40000 ALTER TABLE `fechas_habiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `fechas_habiles` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_FECHAS_I_AUD` AFTER INSERT ON `fechas_habiles` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('FECHAS_HABILES'
        ,'INSERTA'
        ,new.id
        ,CONCAT(new.id, ' - ', new.fec_ini, ' hasta: ', new.fec_fin)
        ,CONCAT('Se insertan nuevas fechas habiles de: ', new.fec_ini, ' hasta: ', new.fec_fin)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_FECHAS_U_AUD` AFTER UPDATE ON `fechas_habiles` FOR EACH ROW --
BEGIN
    --
    IF old.fec_ini != new.fec_ini
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('FECHAS_HABILES'
        ,'MODIFICA'
        ,old.id
        ,old.fec_ini
        ,new.fec_ini
        ,CONCAT('Se modifica la fecha inicial de: ',old.fec_ini,' a: ',new.fec_ini)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
	IF old.fec_fin != new.fec_fin
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('FECHAS_HABILES'
        ,'MODIFICA'
        ,old.id
        ,old.fec_fin
        ,new.fec_fin
        ,CONCAT('Se modifica la fecha final de: ',old.fec_fin,' a: ',new.fec_fin)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_FECHAS_D_AUD` AFTER DELETE ON `fechas_habiles` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('FECHAS_HABILES'
        ,'ELIMINA'
        ,old.id
        ,CONCAT(old.id,' - ', old.fec_ini,' hasta: ',old.fec_fin)
        ,CONCAT('Se eliminan fechas habiles de: ',old.fec_ini,' hasta: ',old.fec_fin)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `parametros`
--

DROP TABLE IF EXISTS `parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parametros` (
  `parametro` varchar(50) NOT NULL COMMENT 'Nombre del parámetro',
  `tipo` decimal(1,0) NOT NULL COMMENT 'Tipo de parámetro',
  `valor` varchar(500) NOT NULL COMMENT 'Valor del parámetro',
  `descripcion` varchar(255) DEFAULT NULL COMMENT 'Descripción del parámetro',
  `sub_parametro` varchar(25) DEFAULT NULL COMMENT 'Subparametro (opcional)',
  UNIQUE KEY `C_PARAMETROS_UK` (`parametro`,`sub_parametro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametros`
--

LOCK TABLES `parametros` WRITE;
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PARAMETROS_I_AUD` AFTER INSERT ON `parametros` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PARAMETROS'
        ,'INSERTA'
        ,new.parametro
        ,NULL
        ,CONCAT('Se inserta el parametro: ', new.parametro,' con sub_parametro: ', new.sub_parametro)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PARAMETROS_U_AUD` AFTER UPDATE ON `parametros` FOR EACH ROW --
BEGIN
    --
    IF old.valor != new.valor
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PARAMETROS'
        ,'MODIFICA'
        ,old.parametro
        ,old.valor
        ,new.valor
        ,CONCAT('Se modifica el valor del parametro: ', old.parametro)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.descripcion != new.descripcion
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PARAMETROS'
        ,'MODIFICA'
        ,old.parametro
        ,old.descripcion
        ,new.descripcion
        ,CONCAT('Se modifica la descripcion del parametro de: ',old.descripcion,' a: ',new.descripcion)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PARAMETROS_D_AUD` AFTER DELETE ON `parametros` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PARAMETROS'
        ,'ELIMINA'
        ,old.parametro
        ,null
        ,CONCAT('Se elimina el parametro: ', old.parametro,' con sub_parametro: ', old.sub_parametro)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `programas`
--

DROP TABLE IF EXISTS `programas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `programas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programas`
--

LOCK TABLES `programas` WRITE;
/*!40000 ALTER TABLE `programas` DISABLE KEYS */;
/*!40000 ALTER TABLE `programas` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PROGRAMAS_I_AUD` AFTER INSERT ON `programas` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROGRAMAS'
        ,'INSERTA'
        ,new.id
        ,NULL
        ,CONCAT('Se inserta el programa: ', new.id,' nombre: ', new.nombre)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PROGRAMAS_U_AUD` AFTER UPDATE ON `programas` FOR EACH ROW --
BEGIN
    --
    IF old.nombre != new.nombre
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROGRAMAS'
        ,'MODIFICA'
        ,old.id
        ,old.nombre
        ,new.nombre
        ,CONCAT('Se modifica el nombre del programa de: ', old.nombre, ' a: ',new.nombre)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PROGRAMAS_D_AUD` AFTER DELETE ON `programas` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROGRAMAS'
        ,'ELIMINA'
        ,old.id
        ,null
        ,CONCAT('Se elimina el programa: ', old.id,' nombre: ', old.nombre)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `proyectos`
--

DROP TABLE IF EXISTS `proyectos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proyectos` (
  `cod_proyecto` varchar(20) NOT NULL COMMENT 'Codigo del proyecto al que se va a realizar el seguimiento',
  `titulo` varchar(50) NOT NULL COMMENT 'Titulo del proyecto al que se va a realizar el seguimiento',
  `programa` varchar(50) NOT NULL COMMENT 'Programa al que pertenece el proyecto',
  `hor_asesoria` varchar(50) DEFAULT NULL COMMENT 'Horario en que se programa la asesoria',
  `cor_director` varchar(40) NOT NULL COMMENT 'Usuario del sistema docente',
  `can_estudiantes` int NOT NULL COMMENT 'Cantidad de estudiantes en el proyecto',
  `cor_estudiante1` varchar(40) NOT NULL COMMENT 'Usuario del sistema estudiante 1',
  `cor_estudiante2` varchar(40) DEFAULT NULL COMMENT 'Usuario del sistema estudiante 2',
  `cor_estudiante3` varchar(40) DEFAULT NULL COMMENT 'Usuario del sistema estudiante 3',
  `tip_proyecto` varchar(40) NOT NULL COMMENT 'Tipo de proyecto PG1 o PG2',
  `estado` tinyint(1) NOT NULL COMMENT 'Estado del proyecto',
  `aval` tinyint(1) DEFAULT NULL COMMENT 'Estado de aprobación del proyecto',
  PRIMARY KEY (`cod_proyecto`),
  KEY `cor_director` (`cor_director`),
  KEY `cor_estudiante1` (`cor_estudiante1`),
  KEY `cor_estudiante2` (`cor_estudiante2`),
  KEY `cor_estudiante3` (`cor_estudiante3`),
  CONSTRAINT `proyectos_ibfk_1` FOREIGN KEY (`cor_director`) REFERENCES `usuarios` (`correo`),
  CONSTRAINT `proyectos_ibfk_2` FOREIGN KEY (`cor_estudiante1`) REFERENCES `usuarios` (`correo`),
  CONSTRAINT `proyectos_ibfk_3` FOREIGN KEY (`cor_estudiante2`) REFERENCES `usuarios` (`correo`),
  CONSTRAINT `proyectos_ibfk_4` FOREIGN KEY (`cor_estudiante3`) REFERENCES `usuarios` (`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla de registro de proyecto Associando los participantes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyectos`
--

LOCK TABLES `proyectos` WRITE;
/*!40000 ALTER TABLE `proyectos` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyectos` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PROYECTOS_I_AUD` AFTER INSERT ON `proyectos` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'INSERTA'
        ,new.cod_proyecto
        ,NULL
        ,CONCAT('Se inserta el proyecto: ', new.cod_proyecto,' - ', new.titulo, ' del programa: ', new.programa)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PROYECTOS_U_AVAL` BEFORE UPDATE ON `proyectos` FOR EACH ROW --
BEGIN
  UPDATE seguimientos 
  SET	 pro_evaluado = new.aval
  WHERE	 cod_proyecto = new.cod_proyecto;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PROYECTOS_U_AUD` AFTER UPDATE ON `proyectos` FOR EACH ROW --
BEGIN
    --
    IF old.hor_asesoria != new.hor_asesoria
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.cod_proyecto
        ,old.hor_asesoria
        ,new.hor_asesoria
        ,CONCAT('Se modifica el horario de la asesoria del proyecto de: ', old.hor_asesoria, ' a: ',new.hor_asesoria)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.cor_director != new.cor_director
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.cod_proyecto
        ,old.cor_director
        ,new.cor_director
        ,CONCAT('Se modifica el director del proyecto de: ', old.cor_director, ' a: ',new.cor_director)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.can_estudiantes != new.can_estudiantes
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.cod_proyecto
        ,old.can_estudiantes
        ,new.can_estudiantes
        ,CONCAT('Se modifica la cantidad de estudiantes del proyecto de: ', old.can_estudiantes, ' a: ',new.can_estudiantes)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.estado != new.estado
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.cod_proyecto
        ,old.estado
        ,new.estado
        ,CONCAT('Se modifica el estado del proyecto de: ', old.estado, ' a: ',new.estado)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_PROYECTOS_D_AUD` AFTER DELETE ON `proyectos` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'ELIMINA'
        ,old.cod_proyecto
        ,null
        ,CONCAT('Se elimina el proyecto: ', old.cod_proyecto,' - ', old.titulo, ' del programa: ', old.programa)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL,
  `rol` varchar(40) NOT NULL COMMENT 'Rol en el sistema para el usuario',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrador'),(2,'Director'),(3,'Estudiante');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seguimientos`
--

DROP TABLE IF EXISTS `seguimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seguimientos` (
  `id_seguimiento` int NOT NULL AUTO_INCREMENT COMMENT 'Codigo unico del registro creado',
  `cod_proyecto` varchar(20) NOT NULL COMMENT 'Codigo del proyecto al que se va a realizar el seguimiento',
  `hor_asesoria` varchar(50) NOT NULL COMMENT 'Codigo del proyecto al que se va a realizar el seguimiento',
  `des_tematica` varchar(4000) NOT NULL COMMENT 'Descripcion del seguimiento',
  `hallazgos` varchar(4000) NOT NULL COMMENT 'Revisiones para el proximo seguimiento',
  `pendientes` varchar(4000) DEFAULT NULL COMMENT 'Revisiones del anterior seguimiento',
  `cor_director` varchar(40) NOT NULL COMMENT 'Usuario del sistema docente',
  `ace_director` varchar(1) DEFAULT NULL COMMENT 'Aceptacion de profesor',
  `asi_director` tinyint(1) DEFAULT NULL COMMENT 'Asistencia de Director',
  `cor_estudiante1` varchar(40) NOT NULL COMMENT 'Usuario del sistema estudiante 1',
  `ace_estudiante1` tinyint(1) DEFAULT NULL COMMENT 'Aceptacion de estudiante 1',
  `cor_estudiante2` varchar(40) DEFAULT NULL COMMENT 'Usuario del sistema estudiante 2',
  `ace_estudiante2` tinyint(1) DEFAULT NULL COMMENT 'Aceptacion de estudiante 2',
  `cor_estudiante3` varchar(40) DEFAULT NULL COMMENT 'Usuario del sistema estudiante 3',
  `ace_estudiante3` tinyint(1) DEFAULT NULL COMMENT 'Aceptacion de estudiante 3',
  `asi_estudiante1` tinyint(1) DEFAULT NULL COMMENT 'Asistencia de estudiante 1',
  `asi_estudiante2` tinyint(1) DEFAULT NULL COMMENT 'Asistencia de estudiante 2',
  `asi_estudiante3` tinyint(1) DEFAULT NULL COMMENT 'Asistencia de estudiante 3',
  `com_estudiante1` varchar(500) DEFAULT NULL COMMENT 'Comentario del estudiante seguimiento',
  `com_estudiante2` varchar(500) DEFAULT NULL COMMENT 'Comentario del estudiante seguimiento',
  `com_estudiante3` varchar(500) DEFAULT NULL COMMENT 'Comentario del estudiante seguimiento',
  `pro_evaluado` tinyint(1) DEFAULT NULL COMMENT 'Evaluación de proyecto',
  PRIMARY KEY (`id_seguimiento`),
  KEY `cor_director` (`cor_director`),
  KEY `cor_estudiante1` (`cor_estudiante1`),
  KEY `cor_estudiante2` (`cor_estudiante2`),
  KEY `cor_estudiante3` (`cor_estudiante3`),
  KEY `cod_proyecto` (`cod_proyecto`),
  CONSTRAINT `seguimientos_ibfk_1` FOREIGN KEY (`cor_director`) REFERENCES `usuarios` (`correo`),
  CONSTRAINT `seguimientos_ibfk_2` FOREIGN KEY (`cor_estudiante1`) REFERENCES `usuarios` (`correo`),
  CONSTRAINT `seguimientos_ibfk_3` FOREIGN KEY (`cor_estudiante2`) REFERENCES `usuarios` (`correo`),
  CONSTRAINT `seguimientos_ibfk_4` FOREIGN KEY (`cor_estudiante3`) REFERENCES `usuarios` (`correo`),
  CONSTRAINT `seguimientos_ibfk_5` FOREIGN KEY (`cod_proyecto`) REFERENCES `proyectos` (`cod_proyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla de registro de seguimiento semanal';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seguimientos`
--

LOCK TABLES `seguimientos` WRITE;
/*!40000 ALTER TABLE `seguimientos` DISABLE KEYS */;
/*!40000 ALTER TABLE `seguimientos` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_SEGUIMIENTOS_I_AUD` AFTER INSERT ON `seguimientos` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('SEGUIMIENTO'
        ,'INSERTA'
        ,new.id_seguimiento
        ,NULL
        ,CONCAT('Se inserta el seguimiento: ', new.id_seguimiento,' del proyecto ', new.cod_proyecto)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_SEGUIMIENTOS_U_AUD` AFTER UPDATE ON `seguimientos` FOR EACH ROW --
BEGIN
    --
    IF old.cod_proyecto != new.cod_proyecto
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('SEGUIMIENTO'
        ,'MODIFICA'
        ,old.id_seguimiento
        ,old.cod_proyecto
        ,new.cod_proyecto
        ,CONCAT('Se modifica el proyecto del seguimiento de: ', old.cod_proyecto, ' a: ',new.cod_proyecto)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.des_tematica != new.des_tematica
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('SEGUIMIENTO'
        ,'MODIFICA'
        ,old.id_seguimiento
        ,old.des_tematica
        ,new.des_tematica
        ,CONCAT('Se modifica la temitaca del seguimiento de: ', old.des_tematica, ' a: ',new.des_tematica)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.hallazgos != new.hallazgos
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('SEGUIMIENTO'
        ,'MODIFICA'
        ,old.id_seguimiento
        ,old.hallazgos
        ,new.hallazgos
        ,CONCAT('Se modifican los hallazgos del seguimiento de: ', old.hallazgos, ' a: ',new.hallazgos)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.pendientes != new.pendientes
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.id_seguimiento
        ,old.pendientes
        ,new.pendientes
        ,CONCAT('Se modifican los pendientes del seguimiento de: ', old.pendientes, ' a: ',new.pendientes)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `T_SEGUIMIENTOS_D_AUD` AFTER DELETE ON `seguimientos` FOR EACH ROW --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'ELIMINA'
        ,old.id_seguimiento
        ,null
        ,CONCAT('Se elimina el seguimiento: ', old.id_seguimiento,' del proyecto ', old.cod_proyecto)
        ,SYSDATE()
        ,(SELECT USER()));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `roleId` int NOT NULL,
  `userId` varchar(255) NOT NULL,
  PRIMARY KEY (`roleId`,`userId`),
  KEY `userId` (`userId`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `usuarios` (`correo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'prueba@udi.edu.co');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `correo` varchar(40) NOT NULL COMMENT 'Correo electronico del usuario',
  `contrasena` varchar(100) DEFAULT NULL COMMENT 'Constraseña encriptada del usuario',
  `nombre` varchar(30) NOT NULL COMMENT 'Nombre del usuario',
  `apellido` varchar(30) NOT NULL COMMENT 'Apellido del usuario',
  `tip_documento` varchar(2) NOT NULL COMMENT 'Tipo de documento del usuario',
  `num_documento` decimal(12,0) NOT NULL COMMENT 'Documento de la persona',
  `telefono` decimal(10,0) NOT NULL COMMENT 'Numero del teléfono celular',
  `estado` tinyint(1) NOT NULL COMMENT 'Estado del usuario (S-Activo/N-No Activo)',
  PRIMARY KEY (`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla para el registro de los usuarios del sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('prueba@udi.edu.co','$2a$10$5Y52XwLkZvpQpEY7R5ZXZOc/CKyUeZv0qNqQH3UffVVuzi7KRGzje','Usuario','prueba','CC',9999999999,9999999999,1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_seguimientos`
--

DROP TABLE IF EXISTS `v_seguimientos`;
/*!50001 DROP VIEW IF EXISTS `v_seguimientos`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_seguimientos` AS SELECT 
 1 AS `cod_proyecto`,
 1 AS `pendientes`,
 1 AS `hor_asesoria`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_usuarios`
--

DROP TABLE IF EXISTS `v_usuarios`;
/*!50001 DROP VIEW IF EXISTS `v_usuarios`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_usuarios` AS SELECT 
 1 AS `correo`,
 1 AS `rol`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'proyecto'
--

--
-- Dumping routines for database 'proyecto'
--
/*!50003 DROP FUNCTION IF EXISTS `F_FECHAS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `F_FECHAS`(vi_proyecto VARCHAR(20)) RETURNS varchar(50) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
  DECLARE vl_fecha VARCHAR(50);
  --
  SELECT IFNULL(CONCAT(DATE_ADD(STR_TO_DATE(SUBSTR(s.hor_asesoria,1,10), '%Y-%m-%d'),INTERVAL 7 DAY),SUBSTR(s.hor_asesoria,11,6))
  ,CONCAT(DATE_ADD(STR_TO_DATE(SUBSTR(p.hor_asesoria,1,10), '%Y-%m-%d'),INTERVAL 7 DAY),SUBSTR(p.hor_asesoria,11,6))) fecha
  INTO   vl_fecha
  FROM   proyectos p 
  ,      seguimientos s
  WHERE  p.cod_proyecto = s.cod_proyecto
  AND    id_seguimiento IN (SELECT MAX(se.id_seguimiento) FROM seguimientos se)
  AND	 p.cod_proyecto = vi_proyecto;
  --
  RETURN vl_fecha;
  --
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `F_MAXIMO_SEGUIMIENTO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `F_MAXIMO_SEGUIMIENTO`(vi_proyecto VARCHAR(20)) RETURNS varchar(4000) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
  DECLARE vl_pendientes VARCHAR(4000);
    --
  SELECT hallazgos
  INTO   vl_pendientes
  FROM   seguimientos s
  WHERE  id_seguimiento IN (SELECT MAX(se.id_seguimiento) FROM seguimientos se)
  AND    cod_proyecto = vi_proyecto;
  RETURN vl_pendientes;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_seguimientos`
--

/*!50001 DROP VIEW IF EXISTS `v_seguimientos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_seguimientos` AS select `p`.`cod_proyecto` AS `cod_proyecto`,`F_MAXIMO_SEGUIMIENTO`(`s`.`cod_proyecto`) AS `pendientes`,(case when (`F_FECHAS`(`s`.`cod_proyecto`) > concat(str_to_date(sysdate(),'%Y-%m-%d'),substr(`F_FECHAS`(`s`.`cod_proyecto`),11,6))) then concat(str_to_date(sysdate(),'%Y-%m-%d'),substr(`F_FECHAS`(`s`.`cod_proyecto`),11,6)) when (`F_FECHAS`(`s`.`cod_proyecto`) < concat(str_to_date(sysdate(),'%Y-%m-%d'),substr(`F_FECHAS`(`s`.`cod_proyecto`),11,6))) then concat(str_to_date(sysdate(),'%Y-%m-%d'),substr(`F_FECHAS`(`s`.`cod_proyecto`),11,6)) else `F_FECHAS`(`s`.`cod_proyecto`) end) AS `hor_asesoria` from (`proyectos` `p` join `seguimientos` `s`) where (`p`.`cod_proyecto` = `s`.`cod_proyecto`) group by `p`.`cod_proyecto`,`F_MAXIMO_SEGUIMIENTO`(`s`.`cod_proyecto`),`hor_asesoria` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_usuarios`
--

/*!50001 DROP VIEW IF EXISTS `v_usuarios`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_usuarios` AS select `s`.`correo` AS `correo`,`r`.`id` AS `rol` from ((`roles` `r` join `user_roles` `u`) join `usuarios` `s`) where ((`r`.`id` = `u`.`roleId`) and (`u`.`userId` = `s`.`correo`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-16 15:03:39
