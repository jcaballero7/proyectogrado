INSERT INTO usuarios
  (correo
  ,contrasena
  ,nombre
  ,apellido
  ,tip_documento
  ,num_documento
  ,telefono
  ,estado)
VALUES
  ('prueba@udi.edu.co'
  ,'$2a$10$5Y52XwLkZvpQpEY7R5ZXZOc/CKyUeZv0qNqQH3UffVVuzi7KRGzje'
  ,'Usuario'
  ,'prueba'
  ,'CC'
  ,'9999999999'
  ,'9999999999'
  ,'1');
--clave QVtBNdhpri

INSERT INTO user_roles
  (roleid
  ,userid)
VALUES
  ('1'
  ,'prueba@udi.edu.co');