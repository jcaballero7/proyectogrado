insert into festivos (FECHA, DESCRIPCION)
values ('2021-01-01', 'A�o Nuevo');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-01-11', 'Dia de los reyes magos');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-03-22', 'Dia de San Jose');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-03-28', 'Domingo de ramos');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-04-01', 'Jueves santo');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-04-02', 'Viernes santo');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-04-04', 'Domingo de Resurrecci�n');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-05-01', 'Dia del trabajador');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-05-17', 'D�a de la Ascensi�n');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-06-07', 'Corpus Christi');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-06-14', 'Sagrado coraz�n de Jesus');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-07-05', 'San pedro y San Pablo');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-07-20', 'D�a de la Independencia');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-08-07', 'Batalla de Boyac�');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-08-16', 'La asunci�n de la virgen');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-10-18', 'Dia de la Raza');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-11-01', 'D�a de todos los Santos');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-11-15', 'Independencia de Cartagena');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-12-08', 'Dia de la inmaculada concepci�n');

insert into festivos (FECHA, DESCRIPCION)
values ('2021-12-25', 'Navidad');

