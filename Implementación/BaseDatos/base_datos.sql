--
--- Crear el esquema
--
CREATE DATABASE proyecto;
--
--- Crear las tablas
--
CREATE table programas (id int AUTO_INCREMENT PRIMARY KEY,
nombre	VARCHAR(50));
--
CREATE table fechas_habiles (id int AUTO_INCREMENT PRIMARY KEY,
fec_ini	VARCHAR(50) NOT NULL,
fec_fin	VARCHAR(50) NOT NULL,
descripcion	VARCHAR(50));
--
create table PARAMETROS ( 
parametro     VARCHAR(50) not null COMMENT 'Nombre del parámetro', 
tipo          NUMERIC(1) not null COMMENT 'Tipo de parámetro', 
valor         VARCHAR(500) not null COMMENT 'Valor del parámetro', 
descripcion   VARCHAR(255) COMMENT 'Descripción del parámetro', 
sub_parametro VARCHAR(25) COMMENT 'Subparametro (opcional)' 
);
alter table PARAMETROS add constraint C_PARAMETROS_UK unique (PARAMETRO, SUB_PARAMETRO);
--
create table roles (id INT PRIMARY KEY,
                   	rol	   varchar(40) not null comment 'Rol en el sistema para el usuario'); 
--
create table usuarios (correo VARCHAR(40) not null COMMENT 'Correo electronico del usuario', 
contrasena VARCHAR(100) COMMENT 'Constraseña encriptada del usuario', 
nombre VARCHAR(30) not null COMMENT 'Nombre del usuario', 
apellido VARCHAR(30) not null COMMENT 'Apellido del usuario',
tip_documento varchar(2)  not null COMMENT 'Tipo de documento del usuario',
num_documento NUMERIC(12) not null COMMENT 'Documento de la persona', 
telefono NUMERIC(10) not null COMMENT 'Numero del teléfono celular',  
estado BOOLEAN not null comment 'Estado del usuario (S-Activo/N-No Activo)')
comment 'Tabla para el registro de los usuarios del sistema';
alter table USUARIOS add constraint CORREO_PK primary key (correo);
--
CREATE TABLE user_roles (
  roleId int NOT NULL,
  userId varchar(255) NOT NULL,
  PRIMARY KEY (roleId,userId),
  KEY userId (userId),
  CONSTRAINT user_roles_ibfk_1 FOREIGN KEY (roleId) REFERENCES roles (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT user_roles_ibfk_2 FOREIGN KEY (userId) REFERENCES usuarios (correo) ON DELETE CASCADE ON UPDATE CASCADE
);
--
--
Create table proyectos
(cod_proyecto   VARCHAR(20) not null comment 'Codigo del proyecto al que se va a realizar el seguimiento', 
titulo		    VARCHAR(50) not null comment 'Titulo del proyecto al que se va a realizar el seguimiento',
programa	    VARCHAR(50) not null comment 'Programa al que pertenece el proyecto',
hor_asesoria    VARCHAR(50) comment 'Horario en que se programa la asesoria',
cor_director    VARCHAR(40) not null comment 'Usuario del sistema docente',
can_estudiantes INT not null comment 'Cantidad de estudiantes en el proyecto',  
cor_estudiante1 VARCHAR(40) not null comment 'Usuario del sistema estudiante 1', 
cor_estudiante2 VARCHAR(40) comment 'Usuario del sistema estudiante 2',
cor_estudiante3 VARCHAR(40) comment 'Usuario del sistema estudiante 3',
tip_proyecto 	VARCHAR(40) not null comment 'Tipo de proyecto PG1 o PG2',
estado			BOOLEAN not null comment 'Estado del proyecto',
aval			BOOLEAN comment 'Estado de aprobación del proyecto')
COMMENT 'Tabla de registro de proyecto Associando los participantes';
--
alter table proyectos add PRIMARY KEY (cod_proyecto);
alter table proyectos add FOREIGN KEY (cor_director) REFERENCES usuarios(correo);
alter table proyectos add FOREIGN KEY (cor_estudiante1) REFERENCES usuarios(correo);
alter table proyectos add FOREIGN KEY (cor_estudiante2) REFERENCES usuarios(correo);
alter table proyectos add FOREIGN KEY (cor_estudiante3) REFERENCES usuarios(correo);
--
Create table seguimientos
(id_seguimiento integer NOT NULL AUTO_INCREMENT PRIMARY KEY comment 'Codigo unico del registro creado',
cod_proyecto    VARCHAR(20) not null comment 'Codigo del proyecto al que se va a realizar el seguimiento',
hor_asesoria    VARCHAR(50) not null comment 'Codigo del proyecto al que se va a realizar el seguimiento',
des_tematica 	VARCHAR(4000) not null comment 'Descripcion del seguimiento',
hallazgos		VARCHAR(4000) not null comment 'Revisiones para el proximo seguimiento',
pendientes		VARCHAR(4000) comment 'Revisiones del anterior seguimiento',
cor_director 	VARCHAR(40) not null comment 'Usuario del sistema docente', 
ace_director   	VARCHAR(1) comment 'Aceptacion de profesor',
asi_director 	BOOLEAN comment 'Asistencia de Director', 
cor_estudiante1 VARCHAR(40) not null comment 'Usuario del sistema estudiante 1',
ace_estudiante1 BOOLEAN comment 'Aceptacion de estudiante 1', 
cor_estudiante2 VARCHAR(40) comment 'Usuario del sistema estudiante 2',
ace_estudiante2 BOOLEAN comment 'Aceptacion de estudiante 2',
cor_estudiante3 VARCHAR(40) comment 'Usuario del sistema estudiante 3',
ace_estudiante3 BOOLEAN comment 'Aceptacion de estudiante 3',
asi_estudiante1 BOOLEAN comment 'Asistencia de estudiante 1',
asi_estudiante2 BOOLEAN comment 'Asistencia de estudiante 2',
asi_estudiante3 BOOLEAN comment 'Asistencia de estudiante 3',
com_estudiante1 VARCHAR(500) comment 'Comentario del estudiante seguimiento',
com_estudiante2 VARCHAR(500) comment 'Comentario del estudiante seguimiento',
com_estudiante3 VARCHAR(500) comment 'Comentario del estudiante seguimiento',
pro_evaluado 	BOOLEAN comment 'Evaluación de proyecto'
)
comment 'Tabla de registro de seguimiento semanal';
--
alter table seguimientos add FOREIGN KEY (cor_director) REFERENCES usuarios(correo);
alter table seguimientos add FOREIGN KEY (cor_estudiante1) REFERENCES usuarios(correo);
alter table seguimientos add FOREIGN KEY (cor_estudiante2) REFERENCES usuarios(correo);
alter table seguimientos add FOREIGN KEY (cor_estudiante3) REFERENCES usuarios(correo);
alter table seguimientos add FOREIGN KEY (cod_proyecto) REFERENCES proyectos(cod_proyecto);
--
create table ANEXOS
(
  id_anexo        INTEGER AUTO_INCREMENT PRIMARY KEY not null comment 'Identificador único del anexo',
  anexo    	      VARCHAR(250) comment 'Descripción del anexo',
  id_seguimiento  INTEGER not null comment 'Identificador único de donde se genera el anexo'
)
comment 'Tabla para el registro anexos';
--
alter table ANEXOS add FOREIGN KEY (id_seguimiento) REFERENCES seguimientos(id_seguimiento);
--
create table auditoria (tabla VARCHAR(30) not null
			,campo	VARCHAR(50)
			,accion VARCHAR(50)
			,llave  VARCHAR(80)
			,val_anterior VARCHAR(200)
			,val_actual   VARCHAR(200)
			,observacion  VARCHAR(200)
			,fec_sistema  DATE not null
            ,usu_sistema VARCHAR(80)
		       );
--
--- Crear los trigger
--
DELIMITER /
CREATE TRIGGER T_FECHAS_I_AUD
    AFTER INSERT ON fechas_habiles
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('FECHAS_HABILES'
        ,'INSERTA'
        ,new.id
        ,CONCAT(new.id, ' - ', new.fec_ini, ' hasta: ', new.fec_fin)
        ,CONCAT('Se insertan nuevas fechas habiles de: ', new.fec_ini, ' hasta: ', new.fec_fin)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_FECHAS_D_AUD
    AFTER DELETE ON fechas_habiles
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('FECHAS_HABILES'
        ,'ELIMINA'
        ,old.id
        ,CONCAT(old.id,' - ', old.fec_ini,' hasta: ',old.fec_fin)
        ,CONCAT('Se eliminan fechas habiles de: ',old.fec_ini,' hasta: ',old.fec_fin)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_FECHAS_U_AUD
    AFTER UPDATE ON fechas_habiles
    FOR EACH ROW
  --
BEGIN
    --
    IF old.fec_ini != new.fec_ini
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('FECHAS_HABILES'
        ,'MODIFICA'
        ,old.id
        ,old.fec_ini
        ,new.fec_ini
        ,CONCAT('Se modifica la fecha inicial de: ',old.fec_ini,' a: ',new.fec_ini)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
	IF old.fec_fin != new.fec_fin
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('FECHAS_HABILES'
        ,'MODIFICA'
        ,old.id
        ,old.fec_fin
        ,new.fec_fin
        ,CONCAT('Se modifica la fecha final de: ',old.fec_fin,' a: ',new.fec_fin)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PARAMETROS_I_AUD
    AFTER INSERT ON parametros
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PARAMETROS'
        ,'INSERTA'
        ,new.parametro
        ,NULL
        ,CONCAT('Se inserta el parametro: ', new.parametro,' con sub_parametro: ', new.sub_parametro)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PARAMETROS_D_AUD
    AFTER DELETE ON parametros
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PARAMETROS'
        ,'ELIMINA'
        ,old.parametro
        ,null
        ,CONCAT('Se elimina el parametro: ', old.parametro,' con sub_parametro: ', old.sub_parametro)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PARAMETROS_U_AUD
    AFTER UPDATE ON parametros
    FOR EACH ROW
  --
BEGIN
    --
    IF old.valor != new.valor
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PARAMETROS'
        ,'MODIFICA'
        ,old.parametro
        ,old.valor
        ,new.valor
        ,CONCAT('Se modifica el valor del parametro: ', old.parametro)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.descripcion != new.descripcion
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PARAMETROS'
        ,'MODIFICA'
        ,old.parametro
        ,old.descripcion
        ,new.descripcion
        ,CONCAT('Se modifica la descripcion del parametro de: ',old.descripcion,' a: ',new.descripcion)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PROYECTOS_I_AUD
    AFTER INSERT ON proyectos
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'INSERTA'
        ,new.cod_proyecto
        ,NULL
        ,CONCAT('Se inserta el proyecto: ', new.cod_proyecto,' - ', new.titulo, ' del programa: ', new.programa)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PROYECTOS_D_AUD
    AFTER DELETE ON proyectos
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'ELIMINA'
        ,old.cod_proyecto
        ,null
        ,CONCAT('Se elimina el proyecto: ', old.cod_proyecto,' - ', old.titulo, ' del programa: ', old.programa)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PROYECTOS_U_AUD
    AFTER UPDATE ON proyectos
    FOR EACH ROW
  --
BEGIN
    --
    IF old.hor_asesoria != new.hor_asesoria
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.cod_proyecto
        ,old.hor_asesoria
        ,new.hor_asesoria
        ,CONCAT('Se modifica el horario de la asesoria del proyecto de: ', old.hor_asesoria, ' a: ',new.hor_asesoria)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.cor_director != new.cor_director
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.cod_proyecto
        ,old.cor_director
        ,new.cor_director
        ,CONCAT('Se modifica el director del proyecto de: ', old.cor_director, ' a: ',new.cor_director)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.can_estudiantes != new.can_estudiantes
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.cod_proyecto
        ,old.can_estudiantes
        ,new.can_estudiantes
        ,CONCAT('Se modifica la cantidad de estudiantes del proyecto de: ', old.can_estudiantes, ' a: ',new.can_estudiantes)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.estado != new.estado
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.cod_proyecto
        ,old.estado
        ,new.estado
        ,CONCAT('Se modifica el estado del proyecto de: ', old.estado, ' a: ',new.estado)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_SEGUIMIENTOS_I_AUD
    AFTER INSERT ON seguimientos
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('SEGUIMIENTO'
        ,'INSERTA'
        ,new.id_seguimiento
        ,NULL
        ,CONCAT('Se inserta el seguimiento: ', new.id_seguimiento,' del proyecto ', new.cod_proyecto)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_SEGUIMIENTOS_D_AUD
    AFTER DELETE ON seguimientos
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'ELIMINA'
        ,old.id_seguimiento
        ,null
        ,CONCAT('Se elimina el seguimiento: ', old.id_seguimiento,' del proyecto ', old.cod_proyecto)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
--
DELIMITER /
CREATE TRIGGER T_SEGUIMIENTOS_U_AUD
    AFTER UPDATE ON seguimientos
    FOR EACH ROW
  --
BEGIN
    --
    IF old.cod_proyecto != new.cod_proyecto
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('SEGUIMIENTO'
        ,'MODIFICA'
        ,old.id_seguimiento
        ,old.cod_proyecto
        ,new.cod_proyecto
        ,CONCAT('Se modifica el proyecto del seguimiento de: ', old.cod_proyecto, ' a: ',new.cod_proyecto)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.des_tematica != new.des_tematica
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('SEGUIMIENTO'
        ,'MODIFICA'
        ,old.id_seguimiento
        ,old.des_tematica
        ,new.des_tematica
        ,CONCAT('Se modifica la temitaca del seguimiento de: ', old.des_tematica, ' a: ',new.des_tematica)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.hallazgos != new.hallazgos
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('SEGUIMIENTO'
        ,'MODIFICA'
        ,old.id_seguimiento
        ,old.hallazgos
        ,new.hallazgos
        ,CONCAT('Se modifican los hallazgos del seguimiento de: ', old.hallazgos, ' a: ',new.hallazgos)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
    --
    IF old.pendientes != new.pendientes
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROYECTO'
        ,'MODIFICA'
        ,old.id_seguimiento
        ,old.pendientes
        ,new.pendientes
        ,CONCAT('Se modifican los pendientes del seguimiento de: ', old.pendientes, ' a: ',new.pendientes)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PROGRAMAS_I_AUD
    AFTER INSERT ON programas
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROGRAMAS'
        ,'INSERTA'
        ,new.id
        ,NULL
        ,CONCAT('Se inserta el programa: ', new.id,' nombre: ', new.nombre)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PROGRAMAS_D_AUD
    AFTER DELETE ON programas
    FOR EACH ROW
  --
BEGIN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROGRAMAS'
        ,'ELIMINA'
        ,old.id
        ,null
        ,CONCAT('Se elimina el programa: ', old.id,' nombre: ', old.nombre)
        ,SYSDATE()
        ,(SELECT USER()));
END
/
DELIMITER ;
--
--
DELIMITER /
CREATE TRIGGER T_PROGRAMAS_U_AUD
    AFTER UPDATE ON programas
    FOR EACH ROW
  --
BEGIN
    --
    IF old.nombre != new.nombre
    THEN
      INSERT INTO auditoria
        (tabla
        ,accion
        ,llave
        ,val_anterior
        ,val_actual
        ,observacion
        ,fec_sistema
        ,usu_sistema)
      VALUES
        ('PROGRAMAS'
        ,'MODIFICA'
        ,old.id
        ,old.nombre
        ,new.nombre
        ,CONCAT('Se modifica el nombre del programa de: ', old.nombre, ' a: ',new.nombre)
        ,SYSDATE()
        ,(SELECT USER()));
	END IF;
END
/
DELIMITER ;
--
DELIMITER /
CREATE TRIGGER T_PROYECTOS_U_AVAL
    BEFORE UPDATE ON proyectos
    FOR EACH ROW
  --
BEGIN
  UPDATE seguimientos 
  SET	 pro_evaluado = new.aval
  WHERE	 cod_proyecto = new.cod_proyecto;
END
/
DELIMITER ;
--
--- Crear las funciones
--
DELIMITER /
CREATE FUNCTION F_MAXIMO_SEGUIMIENTO(vi_proyecto VARCHAR(20)) 
  RETURNS VARCHAR(4000)
    DETERMINISTIC
BEGIN
  DECLARE vl_pendientes VARCHAR(4000);
    --
  SELECT hallazgos
  INTO   vl_pendientes
  FROM   seguimientos s
  WHERE  id_seguimiento IN (SELECT MAX(se.id_seguimiento) FROM seguimientos se)
  AND    cod_proyecto = vi_proyecto;
  RETURN vl_pendientes;
END 
/
DELIMITER ;
--
DELIMITER /
CREATE FUNCTION F_FECHAS(vi_proyecto VARCHAR(20)) 
  RETURNS VARCHAR(50)
    DETERMINISTIC
BEGIN
  DECLARE vl_fecha VARCHAR(50);
  --
  SELECT IFNULL(CONCAT(DATE_ADD(STR_TO_DATE(SUBSTR(s.hor_asesoria,1,10), '%Y-%m-%d'),INTERVAL 7 DAY),SUBSTR(s.hor_asesoria,11,6))
  ,CONCAT(DATE_ADD(STR_TO_DATE(SUBSTR(p.hor_asesoria,1,10), '%Y-%m-%d'),INTERVAL 7 DAY),SUBSTR(p.hor_asesoria,11,6))) fecha
  INTO   vl_fecha
  FROM   proyectos p 
  ,      seguimientos s
  WHERE  p.cod_proyecto = s.cod_proyecto
  AND    id_seguimiento IN (SELECT MAX(se.id_seguimiento) FROM seguimientos se)
  AND	 p.cod_proyecto = vi_proyecto;
  --
  RETURN vl_fecha;
  --
END 
/
DELIMITER ;
--
--- Crear los vistas
--
CREATE OR REPLACE VIEW V_SEGUIMIENTOS AS
SELECT p.cod_proyecto
,      F_MAXIMO_SEGUIMIENTO(s.cod_proyecto) pendientes
,	   CASE 
		 WHEN F_FECHAS(s.cod_proyecto) > CONCAT(STR_TO_DATE(sysdate(), '%Y-%m-%d'),SUBSTR(F_FECHAS(s.cod_proyecto),11,6)) THEN CONCAT(STR_TO_DATE(sysdate(), '%Y-%m-%d'),SUBSTR(F_FECHAS(s.cod_proyecto),11,6))
         WHEN F_FECHAS(s.cod_proyecto) < CONCAT(STR_TO_DATE(sysdate(), '%Y-%m-%d'),SUBSTR(F_FECHAS(s.cod_proyecto),11,6)) THEN CONCAT(STR_TO_DATE(sysdate(), '%Y-%m-%d'),SUBSTR(F_FECHAS(s.cod_proyecto),11,6))
         ELSE F_FECHAS(s.cod_proyecto)
	   END hor_asesoria
FROM   proyectos p 
,      seguimientos s
WHERE  p.cod_proyecto = s.cod_proyecto
GROUP BY p.cod_proyecto
,      	 F_MAXIMO_SEGUIMIENTO(s.cod_proyecto)
,        hor_asesoria;
--
CREATE OR REPLACE VIEW V_USUARIOS AS
SELECT s.correo
,	   r.id rol
FROM   roles        r 
,	   user_roles	u
,      usuarios		s
WHERE  r.id     = u.roleid
and	   u.userid = s.correo;